<?xml version="1.0" ?><!DOCTYPE TS><TS language="fi" version="2.1">
<context>
    <name>AddPrinterTask</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="547"/>
        <source>URI and driver do not match.</source>
        <translation>Osoite ja ohjain eivät täsmää.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="549"/>
        <source>Install hplip first and restart the app to install the driver again.</source>
        <translation>Asenna hplip ensin ja asenna sitten ohjain uudelleen.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="557"/>
        <source>Please select an hplip driver and try again.</source>
        <translation>Valitse hplip -ohjain ja yritä uudelleen.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="570"/>
        <source>URI can&apos;t be empty</source>
        <translation>URL-osoite on tyhjä</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="577"/>
        <source> not found</source>
        <translation>ei löydy</translation>
    </message>
</context>
<context>
    <name>CheckAttributes</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="306"/>
        <source>Check printer settings</source>
        <translation>Tarkista tulostimen asetukset</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="315"/>
        <source>Checking printer settings...</source>
        <translation>Tarkistetaan tulostimen asetuksia...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="318"/>
        <source>Success</source>
        <translation>Onnistui</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="326"/>
        <source>Failed to get printer attributes, error: </source>
        <translation>Tulostimen ominaisuuksien nouto epäonnistui, virhe:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="334"/>
        <source>%1 is disabled</source>
        <translation>%1 poistettu käytöstä</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="343"/>
        <source>is not accepting jobs</source>
        <translation>ei hyväksy työitä</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="348"/>
        <source>Printer settings are ok</source>
        <translation>Tulostimen asetukset ovat kunnossa</translation>
    </message>
</context>
<context>
    <name>CheckConnected</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="211"/>
        <source>Check printer connection</source>
        <translation>Tarkista tulostimen yhteys</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="223"/>
        <source>Checking printer connection...</source>
        <translation>Tarkistetaan tulostimen yhteyttä...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="226"/>
        <source>Success</source>
        <translation>Onnistui</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="263"/>
        <source>Cannot connect to the printer, error: %1</source>
        <translation>Tulostimeen ei voida muodostaa yhteyttä, virhe: %1</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="271"/>
        <source> is not connected, URI: </source>
        <translation>ei ole yhteyttä, osoitteessa:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="280"/>
        <source>%1 does not exist</source>
        <translation>%1 ei ole olemassa</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="289"/>
        <source>Cannot connect to the printer</source>
        <translation>Tulostimeen ei saada yhteyttä</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="295"/>
        <source>The connection is valid</source>
        <translation>Yhteys on kelvollinen</translation>
    </message>
</context>
<context>
    <name>CheckCupsServer</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="103"/>
        <source>Check CUPS server</source>
        <translation>Tarkista CUPS-palvelin</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="111"/>
        <source>Checking CUPS server...</source>
        <translation>Tarkistetaan CUPS-palvelinta...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="117"/>
        <source>CUPS server is invalid</source>
        <translation>CUPS-palvelin ei kelpaa</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="122"/>
        <source>CUPS server is valid</source>
        <translation>CUPS-palvelin on kelvollinen</translation>
    </message>
</context>
<context>
    <name>CheckDriver</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="140"/>
        <source>Checking driver...</source>
        <translation>Tarkistetaan ohjainta...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="143"/>
        <source>Success</source>
        <translation>Onnistui</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="151"/>
        <source>PPD file %1 not found</source>
        <translation>PPD-tiedostoa %1 ei löydy</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="161"/>
        <source>The driver is damaged</source>
        <translation>Ohjain on viallinen</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="177"/>
        <source>Driver filter %1 not found</source>
        <translation>Ohjaimen suodatin %1 ei löydy</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="189"/>
        <source>%1 is not installed, cannot print now</source>
        <translation>%1 ei ole asennettuna, ei voi tulostaa</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="195"/>
        <source>Driver is valid</source>
        <translation>Ohjain on kelvollinen</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="201"/>
        <source>Check driver</source>
        <translation>Tarkista ohjain</translation>
    </message>
</context>
<context>
    <name>CupsMonitor</name>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="74"/>
        <source>Queuing</source>
        <translation>Jonossa</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="75"/>
        <source>Paused</source>
        <translation>Tauko</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="76"/>
        <source>Printing</source>
        <translation>Tulostaa</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="77"/>
        <source>Stopped</source>
        <translation>Pysäytetty</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="78"/>
        <source>Canceled</source>
        <translation>Peruttu</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="79"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="80"/>
        <source>Completed</source>
        <translation>Valmistunut</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="293"/>
        <source>%1 timed out, reason: %2</source>
        <translation>%1 aikakatkaistu, syy: %2</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="307"/>
        <source>%1 printed successfully, please take away the paper in time!</source>
        <translation>%1 tulostettu onnistuneesti, poista paperi!</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="309"/>
        <source>%1 %2, reason: %3</source>
        <translation>%1 %2, syy: %3</translation>
    </message>
</context>
<context>
    <name>DDestination</name>
    <message>
        <location filename="../util/ddestination.cpp" line="148"/>
        <location filename="../util/ddestination.cpp" line="202"/>
        <source>Unknown</source>
        <translation>Tuntematon</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="209"/>
        <source>Idle</source>
        <translation>Joutilas</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="212"/>
        <source>Printing</source>
        <translation>Tulostaa</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="215"/>
        <source>Disabled</source>
        <translation>Estetty</translation>
    </message>
</context>
<context>
    <name>DPrinterSupplyShowDlg</name>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="75"/>
        <source>Ink/Toner Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="81"/>
        <source>Unknown amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="88"/>
        <source>Unable to get the remaining amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="113"/>
        <source>The amounts are estimated, last updated at %1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The amounts are estimated, last updated at%1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="122"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="221"/>
        <source>Unknown</source>
        <translation>Tuntematon</translation>
    </message>
</context>
<context>
    <name>DPrinterTanslator</name>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="33"/>
        <source>Color</source>
        <translation>Väri</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="34"/>
        <location filename="../util/dprintertanslator.cpp" line="35"/>
        <source>Grayscale</source>
        <translation>Harmaasävy</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="38"/>
        <location filename="../util/dprintertanslator.cpp" line="69"/>
        <location filename="../util/dprintertanslator.cpp" line="86"/>
        <location filename="../util/dprintertanslator.cpp" line="107"/>
        <location filename="../util/dprintertanslator.cpp" line="126"/>
        <location filename="../util/dprintertanslator.cpp" line="135"/>
        <source>None</source>
        <translation>Mitään</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="39"/>
        <location filename="../util/dprintertanslator.cpp" line="49"/>
        <source>Draft</source>
        <translation>Vedos</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="40"/>
        <location filename="../util/dprintertanslator.cpp" line="120"/>
        <source>Normal</source>
        <translation>Normaali</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="43"/>
        <location filename="../util/dprintertanslator.cpp" line="44"/>
        <location filename="../util/dprintertanslator.cpp" line="47"/>
        <source>Print Quality</source>
        <translation>Tulostuksen laatu</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="55"/>
        <location filename="../util/dprintertanslator.cpp" line="56"/>
        <source>Auto-Select</source>
        <translation>Automaattinen</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="57"/>
        <location filename="../util/dprintertanslator.cpp" line="58"/>
        <source>Manual Feeder</source>
        <translation>Manuaalinen syöttölaite</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="59"/>
        <location filename="../util/dprintertanslator.cpp" line="73"/>
        <source>Auto</source>
        <translation>Automaattinen</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="48"/>
        <location filename="../util/dprintertanslator.cpp" line="60"/>
        <source>Manual</source>
        <translation>Manuaalinen</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="52"/>
        <location filename="../util/dprintertanslator.cpp" line="53"/>
        <location filename="../util/dprintertanslator.cpp" line="54"/>
        <source>Paper Source</source>
        <translation>Paperin lähde</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="61"/>
        <source>Drawer 1</source>
        <translation>Laatikko 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="62"/>
        <source>Drawer 2</source>
        <translation>Laatikko 2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="63"/>
        <source>Drawer 3</source>
        <translation>Laatikko 3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="64"/>
        <source>Drawer 4</source>
        <translation>Laatikko 4</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="65"/>
        <source>Drawer 5</source>
        <translation>Laatikko 5</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="66"/>
        <source>Envelope Feeder</source>
        <translation>Kirjekuoren syöttölaite</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="67"/>
        <source>Tray1</source>
        <translation>Lokero 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="68"/>
        <source>Unknown</source>
        <translation>Tuntematon</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="72"/>
        <source>MediaType</source>
        <translation>Paperi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="74"/>
        <source>Plain Paper</source>
        <translation>Tavallinen paperi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="75"/>
        <source>Recycled Paper</source>
        <translation>Kierrätetty paperi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="76"/>
        <source>Color Paper</source>
        <translation>Värillinen paperi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="77"/>
        <source>Bond Paper</source>
        <translation>Painatettu paperi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="78"/>
        <source>Heavy Paper 1</source>
        <translation>Raskas paperi 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="79"/>
        <source>Heavy Paper 2</source>
        <translation>Raskas paperi 2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="80"/>
        <source>Heavy Paper 3</source>
        <translation>Raskas paperi 3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="81"/>
        <location filename="../util/dprintertanslator.cpp" line="82"/>
        <source>OHP</source>
        <translation>OHP</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="83"/>
        <source>Labels</source>
        <translation>Tarroja</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="84"/>
        <source>Envelope</source>
        <translation>Kirjekuori</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="85"/>
        <source>Photo Paper</source>
        <translation>Valokuvapaperi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="89"/>
        <location filename="../util/dprintertanslator.cpp" line="90"/>
        <location filename="../util/dprintertanslator.cpp" line="91"/>
        <source>PageSize</source>
        <translation>Sivunkoko</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="92"/>
        <source>Custom</source>
        <translation>Mukautettu</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="95"/>
        <location filename="../util/dprintertanslator.cpp" line="96"/>
        <location filename="../util/dprintertanslator.cpp" line="97"/>
        <source>Duplex</source>
        <translation>Kaksipuoleinen</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="98"/>
        <source>DuplexTumble</source>
        <translation>Kääntörumpu</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="99"/>
        <source>DuplexNoTumble</source>
        <translation>Ei-kääntörumpua</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="100"/>
        <source>ON (Long-edged Binding)</source>
        <translation>PÄÄLLÄ (pitkänreunan nidonta)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="101"/>
        <source>ON (Short-edged Binding)</source>
        <translation>PÄÄLLÄ (lyhyenreunan nidonta)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="102"/>
        <location filename="../util/dprintertanslator.cpp" line="103"/>
        <source>OFF</source>
        <translation>POIS</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="106"/>
        <location filename="../util/dprintertanslator.cpp" line="110"/>
        <source>Binding Edge</source>
        <translation>Sidonta reuna</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="124"/>
        <location filename="../util/dprintertanslator.cpp" line="125"/>
        <source>Staple Location</source>
        <translation>Nidonnan sijainti</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="136"/>
        <location filename="../util/dprintertanslator.cpp" line="137"/>
        <source>Resolution</source>
        <translation>Tarkkuus</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="108"/>
        <source>Left</source>
        <translation>Vasen</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="109"/>
        <source>Top</source>
        <translation>Yläosa</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="113"/>
        <source>Portrait (no rotation)</source>
        <translation>Pysty (ei kiertoa)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="114"/>
        <source>Landscape (90 degrees)</source>
        <translation>Vaaka (90 astetta)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="115"/>
        <source>Reverse landscape (270 degrees)</source>
        <translation>Käänteinen vaaka (270 astetta)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="116"/>
        <source>Reverse portrait (180 degrees)</source>
        <translation>Käänteinen pysty (180 astetta)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="117"/>
        <source>Auto Rotation</source>
        <translation>Automaattinen kierto</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="121"/>
        <source>Reverse</source>
        <translation>Käänteinen</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="127"/>
        <source>Bind</source>
        <translation>Nitoja</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="128"/>
        <source>Bind (none)</source>
        <translation>Nitoja (ei)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="129"/>
        <source>Bind (bottom)</source>
        <translation>Nitoja (alaosa)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="130"/>
        <source>Bind (left)</source>
        <translation>Nitoja (vasen)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="131"/>
        <source>Bind (right)</source>
        <translation>Nitoja (oikea)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="132"/>
        <source>Bind (top)</source>
        <translation>Nitoja (yläosa)</translation>
    </message>
</context>
<context>
    <name>DPrintersShowWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="81"/>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="87"/>
        <source>Printers</source>
        <translation>Tulostimet</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="91"/>
        <source>Add printer</source>
        <translation>Lisää tulostin</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="94"/>
        <source>Delete printer</source>
        <translation>Poista tulostin</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="115"/>
        <source>Shared</source>
        <translation>Jaettu</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="118"/>
        <source>Enabled</source>
        <translation>Käytössä</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="121"/>
        <source>Accept jobs</source>
        <translation>Hyväksy työt</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="125"/>
        <source>Set as default</source>
        <translation>Aseta oletukseksi</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="136"/>
        <source>No Printers</source>
        <translation>Ei tulostimia</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="160"/>
        <source>Location:</source>
        <translation>Sijainti:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="164"/>
        <source>Model:</source>
        <translation>Malli:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="167"/>
        <source>Status:</source>
        <translation>Tila:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="195"/>
        <source>Properties</source>
        <translation>Ominaisuudet</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="205"/>
        <source>Print Queue</source>
        <translation>Tulostusjono</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="215"/>
        <source>Print Test Page</source>
        <translation>Tulosta testisivu</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="237"/>
        <source>Supplies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="262"/>
        <source>No printer configured</source>
        <translation>Tulostinta ei määritettynä</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="267"/>
        <source>Click + to add printers</source>
        <translation>Lisää tulostimia napsauttamalla</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="330"/>
        <source>As print jobs are in process, you cannot rename the printer now, please try later</source>
        <translation>Koska tulostustyöt ovat käynnissä, et voi nimetä tulostinta uudelleen, kokeile myöhemmin</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="334"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="382"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="601"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="360"/>
        <source>Idle</source>
        <translation>Joutilas</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="362"/>
        <source>Printing</source>
        <translation>Tulostaa</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="364"/>
        <source>Disabled</source>
        <translation>Estetty</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="379"/>
        <source>CUPS server is not running, and can’t manage printers.</source>
        <translation>CUPS-palvelin ei ole käynnissä, eikä se pysty hallitsemaan tulostimia.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="559"/>
        <source>Are you sure you want to delete the printer &quot;%1&quot; ?</source>
        <translation>Haluatko varmasti poistaa tulostimen &quot;%1&quot; ?</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="565"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="597"/>
        <source>The name already exists</source>
        <translation>Nimi on jo olemassa</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="616"/>
        <source>You will not be able to reprint the completed jobs if continue. Are you sure you want to rename the printer?</source>
        <translation>Valmiita töitä ei voi tulostaa uudelleen, jos jatketaan. Haluatko varmasti nimetä tulostimen uudelleen?</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="621"/>
        <source>Confirm</source>
        <translation>Vahvista</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="679"/>
        <source>The driver is damaged, please install it again.</source>
        <translation>Ohjain on viallinen, asenna se uudestaan.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="682"/>
        <source>Install Driver</source>
        <translation>Asenna ohjain</translation>
    </message>
</context>
<context>
    <name>DPropertySetDlg</name>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="175"/>
        <source>Print Properties</source>
        <translation>Ominaisuudet</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="176"/>
        <source>Driver</source>
        <translation>Ohjain</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="177"/>
        <source>URI</source>
        <translation>Osoite</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="178"/>
        <source>Location</source>
        <translation>Sijainti</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="179"/>
        <source>Description</source>
        <translation>Kuvaus</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="180"/>
        <source>Color Mode</source>
        <translation>Väritila</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="181"/>
        <source>Orientation</source>
        <translation>Suunta</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="182"/>
        <source>Page Order</source>
        <translation>Sivujärjestys</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="603"/>
        <source>Options conflict!</source>
        <translation>Vaihtoehdot ristiriidassa!</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="606"/>
        <source>Please resolve the conflict first, and then save the changes.</source>
        <translation>Ratkaise ensin ristiriita ja tallenna sitten muutokset.</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="625"/>
        <source>Conflict:</source>
        <translation>Ristiriita:</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="636"/>
        <location filename="../ui/dpropertysetdlg.cpp" line="1123"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="1122"/>
        <source>Invalid URI</source>
        <translation>Virheellinen osoite</translation>
    </message>
</context>
<context>
    <name>DriverManager</name>
    <message>
        <location filename="../vendor/zdrivermanager.cpp" line="791"/>
        <source>EveryWhere driver</source>
        <translation>Yleinen ohjain</translation>
    </message>
</context>
<context>
    <name>InstallDriver</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="374"/>
        <source>Failed to find the driver solution: %1, error: %2</source>
        <translation>Ei löytynyt ohjainta: %1, virhe: %2</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="385"/>
        <source>The solution is invalid</source>
        <translation>Ratkaisu ei kelpaa</translation>
    </message>
</context>
<context>
    <name>InstallDriverWindow</name>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="123"/>
        <source>Select a driver from</source>
        <translation>Valitse ohjain</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="127"/>
        <source>Local driver</source>
        <translation>Paikallinen ohjain</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="128"/>
        <source>Local PPD file</source>
        <translation>Paikallinen PPD-tiedosto</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="129"/>
        <source>Search for a driver</source>
        <translation>Etsi ohjainta</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="143"/>
        <location filename="../ui/installdriverwindow.cpp" line="421"/>
        <source>Choose a local driver</source>
        <translation>Valitse paikallinen ohjain</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="146"/>
        <source>Vendor</source>
        <translation>Valmistaja</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="158"/>
        <source>Model</source>
        <translation>Malli</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="169"/>
        <location filename="../ui/installdriverwindow.cpp" line="238"/>
        <source>Driver</source>
        <translation>Ohjain</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="223"/>
        <source>Vendor and Model</source>
        <translation>Valmistaja ja malli</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="225"/>
        <source>Enter a complete vendor and model (Only letters, numbers and whitespaces)</source>
        <translation>Anna täydellinen valmistaja ja malli (vain kirjaimet, numerot ja välilyönnit)</translation>
    </message>
    <message>
        <source>Enter a complete vendor and model</source>
        <translation type="vanished">Anna täydellinen valmistaja ja malli</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="227"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="268"/>
        <source>Install Driver</source>
        <translation>Asenna ohjain</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="403"/>
        <location filename="../ui/installdriverwindow.cpp" line="515"/>
        <source>Reselect</source>
        <translation>Uudelleen</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="429"/>
        <location filename="../ui/installdriverwindow.cpp" line="512"/>
        <source>Select a PPD file</source>
        <translation>Valitse PPD-tiedosto</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="438"/>
        <source>Search for printer driver</source>
        <translation>Etsi tulostinohjain</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="494"/>
        <source>(recommended)</source>
        <translation>(suositus)</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="656"/>
        <source> (recommended)</source>
        <translation>(suositeltavaa)</translation>
    </message>
</context>
<context>
    <name>InstallInterface</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="252"/>
        <source>is invalid</source>
        <translation>on virheellinen</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="281"/>
        <source>Failed to install the driver by calling dbus interface</source>
        <translation>Ohjaimen asennus epäonnistui kutsumalla dbus-rajapintaa</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="326"/>
        <source>Failed to install %1</source>
        <translation>Kohteen %1 asentaminen epäonnistui</translation>
    </message>
</context>
<context>
    <name>InstallPrinterWindow</name>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="85"/>
        <location filename="../ui/installprinterwindow.cpp" line="148"/>
        <source>Installing driver...</source>
        <translation>Asennetaan ohjainta...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="95"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="98"/>
        <location filename="../ui/installprinterwindow.cpp" line="171"/>
        <location filename="../ui/installprinterwindow.cpp" line="188"/>
        <source>View Printer</source>
        <translation>Näytä tulostin</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="100"/>
        <location filename="../ui/installprinterwindow.cpp" line="173"/>
        <location filename="../ui/installprinterwindow.cpp" line="190"/>
        <source>Print Test Page</source>
        <translation>Tulosta testisivu</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="160"/>
        <source>Successfully installed </source>
        <translation>Asennus onnistui</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="166"/>
        <location filename="../ui/installprinterwindow.cpp" line="184"/>
        <source>You have successfully added the printer. Print a test page to check if it works properly.</source>
        <translation>Tulostin on lisätty onnistuneesti. Tulosta testisivu tarkistaaksesi, toimiiko se oikein.</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="178"/>
        <source>Printing test page...</source>
        <translation>Tulostetaan testisivua...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="195"/>
        <source>Did you print the test page successfully?</source>
        <translation>Tulostitko testisivun onnistuneesti?</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="201"/>
        <source>If succeeded, click Yes; if failed, click No</source>
        <translation>Onnistui, valitse Kyllä; Jos toiminto epäonnistui, valitse Ei</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="204"/>
        <source>No</source>
        <translation>Ei</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="206"/>
        <source>Yes</source>
        <translation>Kyllä</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="211"/>
        <location filename="../ui/installprinterwindow.cpp" line="231"/>
        <source>Print failed</source>
        <translation>Tulostaminen epäonnistui</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="217"/>
        <source>Click Reinstall to install the printer driver again, or click Troubleshoot to start troubleshooting.</source>
        <translation>Asenna tulostinohjain uudelleen napsauttamalla tai aloita vianetsintä napsauttamalla Vianmääritys.</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="220"/>
        <location filename="../ui/installprinterwindow.cpp" line="241"/>
        <source>Reinstall</source>
        <translation>Uudelleen</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="222"/>
        <location filename="../ui/installprinterwindow.cpp" line="243"/>
        <source>Troubleshoot</source>
        <translation>Vianmääritys</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="229"/>
        <source>Installation failed</source>
        <translation>Asennus epäonnistui</translation>
    </message>
</context>
<context>
    <name>JobListView</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="214"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="371"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="215"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="372"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="216"/>
        <source>Pause</source>
        <translation>Tauko</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="217"/>
        <source>Resume</source>
        <translation>Jatka</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="218"/>
        <source>Print first</source>
        <translation>Tulosta ensin</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="219"/>
        <source>Reprint</source>
        <translation>Uusinta</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="221"/>
        <source>No print jobs</source>
        <translation>Ei tulostustöitä</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="365"/>
        <source>Are you sure you want to delete this job?</source>
        <translation>Haluatko varmasti poistaa tämän työn?</translation>
    </message>
</context>
<context>
    <name>JobManager</name>
    <message>
        <location filename="../vendor/zjobmanager.cpp" line="279"/>
        <source> not found</source>
        <translation>ei löydy</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1007"/>
        <source>Refresh</source>
        <translation>Virkistä</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1011"/>
        <source>All</source>
        <translation>Kaikki</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1012"/>
        <source>Print Queue</source>
        <translation>Tulostusjono</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1013"/>
        <source>Completed</source>
        <translation>Valmistunut</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1070"/>
        <source> failed</source>
        <translation>epäonnistui</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1091"/>
        <source>%1 jobs</source>
        <translation>%1 työtä</translation>
    </message>
</context>
<context>
    <name>JobsDataModel</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="631"/>
        <source>Job</source>
        <translation>Työ</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="633"/>
        <source>User</source>
        <translation>Käyttäjä</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="635"/>
        <source>Document</source>
        <translation>Asiakirja</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="637"/>
        <source>Printer</source>
        <translation>Tulostin</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="639"/>
        <source>Size</source>
        <translation>Koko</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="641"/>
        <source>Time submitted</source>
        <translation>Lähetysaika</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="643"/>
        <source>Status</source>
        <translation>Tila</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="645"/>
        <source>Action</source>
        <translation>Toiminta</translation>
    </message>
</context>
<context>
    <name>PermissionsWindow</name>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="48"/>
        <source>Connect to %1 to find a printer</source>
        <translation>Yhdistä %1 löytääksesi tulostimen</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="74"/>
        <source>Username</source>
        <translation>Käyttäjätunnus</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="76"/>
        <source>Group</source>
        <translation>Ryhmä</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="78"/>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="92"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="93"/>
        <source>Connect</source>
        <translation>Yhdistä</translation>
    </message>
</context>
<context>
    <name>PrinterApplication</name>
    <message>
        <location filename="../ui/printerapplication.cpp" line="111"/>
        <source>Print Manager</source>
        <translation>Tulostimet</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="112"/>
        <source>Print Manager is a printer management tool, which supports adding and removing printers, managing print jobs and so on.</source>
        <translation>Tulostimet on hallintatyökalu, jolla asennat ja poistat tulostimet, sekä hallitset asetukset.</translation>
    </message>
</context>
<context>
    <name>PrinterSearchWindow</name>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="83"/>
        <location filename="../ui/printersearchwindow.cpp" line="86"/>
        <source>Discover printer</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="89"/>
        <location filename="../ui/printersearchwindow.cpp" line="92"/>
        <source>Find printer</source>
        <translation>Osoita</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="94"/>
        <location filename="../ui/printersearchwindow.cpp" line="97"/>
        <source>Enter URI</source>
        <translation>Anna IP</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="118"/>
        <source>Select a printer</source>
        <translation>Valitse tulostin</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="124"/>
        <source>Refresh</source>
        <translation>Virkistä</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="162"/>
        <source>Install Driver</source>
        <translation>Asenna ohjain</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="190"/>
        <source>Address</source>
        <translation>Osoite</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="192"/>
        <source>Enter an address</source>
        <translation>Anna osoite</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="193"/>
        <source>Find</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="266"/>
        <source>URI</source>
        <translation>Osoite</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="270"/>
        <source>Enter device URI</source>
        <translation>Anna laitteen IP</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="281"/>
        <source>Examples:</source>
        <translation>Esimerkkejä:</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="556"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="644"/>
        <source> (recommended)</source>
        <translation>(suositus)</translation>
    </message>
</context>
<context>
    <name>PrinterTestJob</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="369"/>
        <source>Check test page printing</source>
        <translation>Tarkista testisivun tulostus</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="416"/>
        <source>Printing test page...</source>
        <translation>Tulostetaan testisivua...</translation>
    </message>
</context>
<context>
    <name>PrinterTestPageDialog</name>
    <message>
        <location filename="../ui/printertestpagedialog.cpp" line="63"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="103"/>
        <source>1 min ago</source>
        <translation>1 minuutti sitten</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="106"/>
        <source>%1 mins ago</source>
        <translation>%1 minuuttia sitten</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="110"/>
        <source>1 hr ago</source>
        <translation>1 tunti sitten</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="112"/>
        <source>%1 hrs ago</source>
        <translation>%1 tuntia sitten</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="117"/>
        <source>Yesterday</source>
        <translation>Eilen</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="119"/>
        <source>%1 days ago</source>
        <translation>%1 päivää sitten</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="481"/>
        <source>(use %1 protocol)</source>
        <translation>(käytä %1 -protokollaa)</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="26"/>
        <source>Select a driver</source>
        <translation>Valitse ohjain</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="32"/>
        <source>Driver</source>
        <translation>Ohjain</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="33"/>
        <source>Next</source>
        <translation>Seuraava</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="34"/>
        <source>Install Driver</source>
        <translation>Asenna ohjain</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="36"/>
        <source>Drag a PPD file here 
 or</source>
        <translation>Vedä PPD-tiedosto tähän 
 tai</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="37"/>
        <source>Select a PPD file</source>
        <translation>Valitse PPD-tiedosto</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="39"/>
        <source>Troubleshoot</source>
        <translation>Vianmääritys</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="40"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../vendor/common.cpp" line="149"/>
        <source> not found, please ask the administrator for help</source>
        <translation>ei löydy, kysy järjestelmänvalvojalta apua</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="121"/>
        <source>Direct-attached Device</source>
        <translation>Suoraankytketty laite</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="122"/>
        <source>File</source>
        <translation>Tiedosto</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="255"/>
        <source>Black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="256"/>
        <source>Blue</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="257"/>
        <source>Brown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="258"/>
        <source>Cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="259"/>
        <source>Dark-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="260"/>
        <source>Dark gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="261"/>
        <source>Dark-yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="262"/>
        <source>Dark yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="263"/>
        <source>Gold</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="264"/>
        <source>Gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="265"/>
        <source>Green</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="266"/>
        <source>Light-black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="267"/>
        <source>Light black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="268"/>
        <source>Light-cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="269"/>
        <source>Light cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="270"/>
        <source>Light-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="271"/>
        <source>Light gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="272"/>
        <source>Light-magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="273"/>
        <source>Light magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="274"/>
        <source>Magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="275"/>
        <source>Orange</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="276"/>
        <source>Red</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="277"/>
        <source>Silver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="278"/>
        <source>White</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="279"/>
        <source>Yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="280"/>
        <source>Waste</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RenamePrinterWindow</name>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="84"/>
        <source>Rename Printer</source>
        <translation>Nimeä tulostin</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="91"/>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="93"/>
        <source>Location</source>
        <translation>Sijainti</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="95"/>
        <source>Description</source>
        <translation>Kuvaus</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="106"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="107"/>
        <source>Confirm</source>
        <translation>Vahvista</translation>
    </message>
</context>
<context>
    <name>ServerSettingsWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="85"/>
        <source>Basic Server Settings</source>
        <translation>Palvelimen perusasetukset</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="93"/>
        <source>Publish shared printers connected to this system</source>
        <translation>Jaa tähän järjestelmään kytkettyjä tulostimia</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="94"/>
        <source>Allow printing from the Internet</source>
        <translation>Salli tulostus Internetistä</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="96"/>
        <source>Allow remote administration</source>
        <translation>Salli etähallinta</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="98"/>
        <source>Save debugging information for troubleshooting</source>
        <translation>Tallenna vianetsintätiedot vianetsintää varten</translation>
    </message>
</context>
<context>
    <name>TroubleShootDialog</name>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="109"/>
        <source>Troubleshoot: </source>
        <translation>Vianmääritys:</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="134"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="165"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
</TS>