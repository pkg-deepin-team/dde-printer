<?xml version="1.0" ?><!DOCTYPE TS><TS language="ar" version="2.1">
<context>
    <name>AddPrinterTask</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="547"/>
        <source>URI and driver do not match.</source>
        <translation>الرابط URI و المشغل غير متوافقان.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="549"/>
        <source>Install hplip first and restart the app to install the driver again.</source>
        <translation>ثبت hplip أولا ثم أعد تشغيل البرنامج لتثبيت المشغل مجددا.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="557"/>
        <source>Please select an hplip driver and try again.</source>
        <translation>الرجاء تعيين مشغل hplip ثم إعادة المحاولة.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="570"/>
        <source>URI can&apos;t be empty</source>
        <translation>الرابط URI لا يمكن أن يكون فارغ</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="577"/>
        <source> not found</source>
        <translation>غير موجود</translation>
    </message>
</context>
<context>
    <name>CheckAttributes</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="306"/>
        <source>Check printer settings</source>
        <translation>تأكد من تعديلات الطابعة</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="315"/>
        <source>Checking printer settings...</source>
        <translation>التأكد من تعديلات الطابعة</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="318"/>
        <source>Success</source>
        <translation>تم بنجاح</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="326"/>
        <source>Failed to get printer attributes, error: </source>
        <translation>لم يتم الحصول على خصائص الطابعة، الخطأ:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="334"/>
        <source>%1 is disabled</source>
        <translation>تم تعطيل %1</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="343"/>
        <source>is not accepting jobs</source>
        <translation>لا يقبل عمليات طبع</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="348"/>
        <source>Printer settings are ok</source>
        <translation>تعديلات الطابعة مناسبة</translation>
    </message>
</context>
<context>
    <name>CheckConnected</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="211"/>
        <source>Check printer connection</source>
        <translation>تأكد من ربط الطابعة</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="223"/>
        <source>Checking printer connection...</source>
        <translation>التأكد من ربط الطابعة</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="226"/>
        <source>Success</source>
        <translation>تم بنجاح</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="263"/>
        <source>Cannot connect to the printer, error: %1</source>
        <translation>لا يمكن الإتصال بالطابعة، خطأ: 1%</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="271"/>
        <source> is not connected, URI: </source>
        <translation>غير متصل، الرابط URI:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="280"/>
        <source>%1 does not exist</source>
        <translation>%1 غير موجود</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="289"/>
        <source>Cannot connect to the printer</source>
        <translation>لا يمكن الإتصال بالطابعة</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="295"/>
        <source>The connection is valid</source>
        <translation>الإتصال مقبول</translation>
    </message>
</context>
<context>
    <name>CheckCupsServer</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="103"/>
        <source>Check CUPS server</source>
        <translation>تأكد من الخادم CUPS</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="111"/>
        <source>Checking CUPS server...</source>
        <translation>التأكد من الخادم CUPS...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="117"/>
        <source>CUPS server is invalid</source>
        <translation>الخادم CUPS غير مقبول</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="122"/>
        <source>CUPS server is valid</source>
        <translation>الخادم CUPS مقبول</translation>
    </message>
</context>
<context>
    <name>CheckDriver</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="140"/>
        <source>Checking driver...</source>
        <translation>التأكد من المشغل...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="143"/>
        <source>Success</source>
        <translation>تم بنجاح</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="151"/>
        <source>PPD file %1 not found</source>
        <translation>ملف PPD %1 غير موجود</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="161"/>
        <source>The driver is damaged</source>
        <translation>التعريف معطوب</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="177"/>
        <source>Driver filter %1 not found</source>
        <translation>لم يتم العثور على عامل تصفية محرك الأقراص %1</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="189"/>
        <source>%1 is not installed, cannot print now</source>
        <translation>%1 غير مثبت، لا يمكن الطبع حاليا</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="195"/>
        <source>Driver is valid</source>
        <translation>المشغل مقبول</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="201"/>
        <source>Check driver</source>
        <translation>تأكد من المشغل</translation>
    </message>
</context>
<context>
    <name>CupsMonitor</name>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="74"/>
        <source>Queuing</source>
        <translation>ترتيب</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="75"/>
        <source>Paused</source>
        <translation>توقف مؤقت</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="76"/>
        <source>Printing</source>
        <translation>طبع</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="77"/>
        <source>Stopped</source>
        <translation>متوقف</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="78"/>
        <source>Canceled</source>
        <translation>ملغى</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="79"/>
        <source>Error</source>
        <translation>خطأ</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="80"/>
        <source>Completed</source>
        <translation>تمت العملية</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="293"/>
        <source>%1 timed out, reason: %2</source>
        <translation>إنتهى وقت %1، والسبب هو %2</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="307"/>
        <source>%1 printed successfully, please take away the paper in time!</source>
        <translation>تم طباعة %1 بنجاح، يرجى سحب الأوراق  المطبوعة في الوقت المحدد!</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="309"/>
        <source>%1 %2, reason: %3</source>
        <translation>%1 %2، السبب: 3%</translation>
    </message>
</context>
<context>
    <name>DDestination</name>
    <message>
        <location filename="../util/ddestination.cpp" line="148"/>
        <location filename="../util/ddestination.cpp" line="202"/>
        <source>Unknown</source>
        <translation>غير معروف</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="209"/>
        <source>Idle</source>
        <translation>قيد اﻹسبات</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="212"/>
        <source>Printing</source>
        <translation>طبع</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="215"/>
        <source>Disabled</source>
        <translation>معطل</translation>
    </message>
</context>
<context>
    <name>DPrinterSupplyShowDlg</name>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="75"/>
        <source>Ink/Toner Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="81"/>
        <source>Unknown amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="88"/>
        <source>Unable to get the remaining amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="113"/>
        <source>The amounts are estimated, last updated at %1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The amounts are estimated, last updated at%1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="122"/>
        <source>OK</source>
        <translation>جيد</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="221"/>
        <source>Unknown</source>
        <translation>غير معروف</translation>
    </message>
</context>
<context>
    <name>DPrinterTanslator</name>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="33"/>
        <source>Color</source>
        <translation>لون</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="34"/>
        <location filename="../util/dprintertanslator.cpp" line="35"/>
        <source>Grayscale</source>
        <translation>سلم الرمادي</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="38"/>
        <location filename="../util/dprintertanslator.cpp" line="69"/>
        <location filename="../util/dprintertanslator.cpp" line="86"/>
        <location filename="../util/dprintertanslator.cpp" line="107"/>
        <location filename="../util/dprintertanslator.cpp" line="126"/>
        <location filename="../util/dprintertanslator.cpp" line="135"/>
        <source>None</source>
        <translation>لا شيء</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="39"/>
        <location filename="../util/dprintertanslator.cpp" line="49"/>
        <source>Draft</source>
        <translation>مسودة</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="40"/>
        <location filename="../util/dprintertanslator.cpp" line="120"/>
        <source>Normal</source>
        <translation>عادي</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="43"/>
        <location filename="../util/dprintertanslator.cpp" line="44"/>
        <location filename="../util/dprintertanslator.cpp" line="47"/>
        <source>Print Quality</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="55"/>
        <location filename="../util/dprintertanslator.cpp" line="56"/>
        <source>Auto-Select</source>
        <translation>تحديد أوتوماتيكي</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="57"/>
        <location filename="../util/dprintertanslator.cpp" line="58"/>
        <source>Manual Feeder</source>
        <translation>وحدة التغذية اليدوية</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="59"/>
        <location filename="../util/dprintertanslator.cpp" line="73"/>
        <source>Auto</source>
        <translation>أوتوماتيكي</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="48"/>
        <location filename="../util/dprintertanslator.cpp" line="60"/>
        <source>Manual</source>
        <translation>يدوي</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="52"/>
        <location filename="../util/dprintertanslator.cpp" line="53"/>
        <location filename="../util/dprintertanslator.cpp" line="54"/>
        <source>Paper Source</source>
        <translation>مصدر الورق</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="61"/>
        <source>Drawer 1</source>
        <translation>الراسم 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="62"/>
        <source>Drawer 2</source>
        <translation>الراسم 2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="63"/>
        <source>Drawer 3</source>
        <translation>الراسم 3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="64"/>
        <source>Drawer 4</source>
        <translation>الراسم 4</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="65"/>
        <source>Drawer 5</source>
        <translation>الراسم 5</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="66"/>
        <source>Envelope Feeder</source>
        <translation>مغلف التغذية</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="67"/>
        <source>Tray1</source>
        <translation>الدرج1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="68"/>
        <source>Unknown</source>
        <translation>مجهول</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="72"/>
        <source>MediaType</source>
        <translation>نوع الوسائط</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="74"/>
        <source>Plain Paper</source>
        <translation>ورقة فارغة</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="75"/>
        <source>Recycled Paper</source>
        <translation>ورق معاد تكوينه</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="76"/>
        <source>Color Paper</source>
        <translation>لون الورق</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="77"/>
        <source>Bond Paper</source>
        <translation>ورق السندات (متين)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="78"/>
        <source>Heavy Paper 1</source>
        <translation>ورق ثقيل 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="79"/>
        <source>Heavy Paper 2</source>
        <translation>ورق ثقيل 2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="80"/>
        <source>Heavy Paper 3</source>
        <translation>ورق ثقيل 3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="81"/>
        <location filename="../util/dprintertanslator.cpp" line="82"/>
        <source>OHP</source>
        <translation>جهاز العرض العلوي</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="83"/>
        <source>Labels</source>
        <translation>المسميات</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="84"/>
        <source>Envelope</source>
        <translation>غطاء</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="85"/>
        <source>Photo Paper</source>
        <translation>ورق للصور</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="89"/>
        <location filename="../util/dprintertanslator.cpp" line="90"/>
        <location filename="../util/dprintertanslator.cpp" line="91"/>
        <source>PageSize</source>
        <translation>أبعاد الصفحة</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="92"/>
        <source>Custom</source>
        <translation>معدل</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="95"/>
        <location filename="../util/dprintertanslator.cpp" line="96"/>
        <location filename="../util/dprintertanslator.cpp" line="97"/>
        <source>Duplex</source>
        <translation>الطباعة على وجهين</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="98"/>
        <source>DuplexTumble</source>
        <translation>تراجع مزدوج</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="99"/>
        <source>DuplexNoTumble</source>
        <translation>DuplexNoTumble</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="100"/>
        <source>ON (Long-edged Binding)</source>
        <translation>تشغيل (ربط طويل الحافة)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="101"/>
        <source>ON (Short-edged Binding)</source>
        <translation>تشغيل (ربط قصير الحافة)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="102"/>
        <location filename="../util/dprintertanslator.cpp" line="103"/>
        <source>OFF</source>
        <translation>ايقاف</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="106"/>
        <location filename="../util/dprintertanslator.cpp" line="110"/>
        <source>Binding Edge</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="124"/>
        <location filename="../util/dprintertanslator.cpp" line="125"/>
        <source>Staple Location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="136"/>
        <location filename="../util/dprintertanslator.cpp" line="137"/>
        <source>Resolution</source>
        <translation>الدقة</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="108"/>
        <source>Left</source>
        <translation>اليسار</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="109"/>
        <source>Top</source>
        <translation>الأعلى</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="113"/>
        <source>Portrait (no rotation)</source>
        <translation>عمودي (بدون دوران)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="114"/>
        <source>Landscape (90 degrees)</source>
        <translation>أفقي (90 درجة)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="115"/>
        <source>Reverse landscape (270 degrees)</source>
        <translation>أفقي عكسي (270 درجة)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="116"/>
        <source>Reverse portrait (180 degrees)</source>
        <translation> عمودي عكسي (180 درجة)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="117"/>
        <source>Auto Rotation</source>
        <translation>تدوير أوتوماتيكي</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="121"/>
        <source>Reverse</source>
        <translation>مقلوب</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="127"/>
        <source>Bind</source>
        <translation>ربط</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="128"/>
        <source>Bind (none)</source>
        <translation>ربط (بلا)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="129"/>
        <source>Bind (bottom)</source>
        <translation>ربط (من ألاسفل)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="130"/>
        <source>Bind (left)</source>
        <translation>ربط (من اليسار)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="131"/>
        <source>Bind (right)</source>
        <translation>ربط (من اليمين)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="132"/>
        <source>Bind (top)</source>
        <translation>ربط (من فوق)</translation>
    </message>
</context>
<context>
    <name>DPrintersShowWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="81"/>
        <source>Settings</source>
        <translation>التعديلات</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="87"/>
        <source>Printers</source>
        <translation>الطابعات</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="91"/>
        <source>Add printer</source>
        <translation>إضافة طابعة</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="94"/>
        <source>Delete printer</source>
        <translation>حذف طابعة</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="115"/>
        <source>Shared</source>
        <translation>مشترك</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="118"/>
        <source>Enabled</source>
        <translation>ممكّن</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="121"/>
        <source>Accept jobs</source>
        <translation>قبول العمليات</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="125"/>
        <source>Set as default</source>
        <translation>تعيين كافتراضي</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="136"/>
        <source>No Printers</source>
        <translation>عدم وجود الطابعات</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="160"/>
        <source>Location:</source>
        <translation>المكان:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="164"/>
        <source>Model:</source>
        <translation>نموذج:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="167"/>
        <source>Status:</source>
        <translation>الحالة :</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="195"/>
        <source>Properties</source>
        <translation>الخاصيات</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="205"/>
        <source>Print Queue</source>
        <translation>قائمة الطبع</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="215"/>
        <source>Print Test Page</source>
        <translation>طبع صفحة مسودة</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="237"/>
        <source>Supplies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="262"/>
        <source>No printer configured</source>
        <translation>لا توجد طابعة مبرمجة</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="267"/>
        <source>Click + to add printers</source>
        <translation>إضغط على + لإضافة طابعات</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="330"/>
        <source>As print jobs are in process, you cannot rename the printer now, please try later</source>
        <translation>لا يمكن تغيير إسم الطابعة لأن عملية الطبع جارية، حاول لاحقا</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="334"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="382"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="601"/>
        <source>OK</source>
        <translation>جيد</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="360"/>
        <source>Idle</source>
        <translation>قيد اﻹسبات</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="362"/>
        <source>Printing</source>
        <translation>طبع</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="364"/>
        <source>Disabled</source>
        <translation>معطل</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="379"/>
        <source>CUPS server is not running, and can’t manage printers.</source>
        <translation>خادم CUPS ليس قيد التشغيل ولا يمكنه إدارة الطابعات.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="559"/>
        <source>Are you sure you want to delete the printer &quot;%1&quot; ?</source>
        <translation>هل أنت متأكد من حذف الطابعة &quot;1%&quot; ؟</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="565"/>
        <source>Delete</source>
        <translation>حذف</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="597"/>
        <source>The name already exists</source>
        <translation>الإسم موجود</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="616"/>
        <source>You will not be able to reprint the completed jobs if continue. Are you sure you want to rename the printer?</source>
        <translation>لن تتمكن من تكرار عمليات الطبع التامة إن واصلت. هل أنت متأكد من تغيير إسم الطابعة ؟</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="621"/>
        <source>Confirm</source>
        <translation>تأكيد</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="679"/>
        <source>The driver is damaged, please install it again.</source>
        <translation>ملف التعريف تالف ، يرجى تثبيته مرة أخرى.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="682"/>
        <source>Install Driver</source>
        <translation>تثبيت محرك الأقراص</translation>
    </message>
</context>
<context>
    <name>DPropertySetDlg</name>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="175"/>
        <source>Print Properties</source>
        <translation>خصائص الطابعة</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="176"/>
        <source>Driver</source>
        <translation>المشغل أو برنامج التعريف</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="177"/>
        <source>URI</source>
        <translation>الرابط URI</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="178"/>
        <source>Location</source>
        <translation>المكان</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="179"/>
        <source>Description</source>
        <translation>الوصف</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="180"/>
        <source>Color Mode</source>
        <translation>وضع اللون</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="181"/>
        <source>Orientation</source>
        <translation>الاتجاه</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="182"/>
        <source>Page Order</source>
        <translation>ترتيب الصفحات</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="603"/>
        <source>Options conflict!</source>
        <translation>هنالك تعارض في الخيارات!</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="606"/>
        <source>Please resolve the conflict first, and then save the changes.</source>
        <translation>يُرجى حل التعارض أولاً ، ثم حفظ التغييرات.</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="625"/>
        <source>Conflict:</source>
        <translation>التعارض:</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="636"/>
        <location filename="../ui/dpropertysetdlg.cpp" line="1123"/>
        <source>OK</source>
        <translation>جيد</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="1122"/>
        <source>Invalid URI</source>
        <translation>الرابط URI غير مقبول</translation>
    </message>
</context>
<context>
    <name>DriverManager</name>
    <message>
        <location filename="../vendor/zdrivermanager.cpp" line="791"/>
        <source>EveryWhere driver</source>
        <translation>ملف تعريف EveryWhere</translation>
    </message>
</context>
<context>
    <name>InstallDriver</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="374"/>
        <source>Failed to find the driver solution: %1, error: %2</source>
        <translation>لم يتم إيجاد حل لملف التعريف: 1%، خطأ: 2%</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="385"/>
        <source>The solution is invalid</source>
        <translation>الحل غير مقبول</translation>
    </message>
</context>
<context>
    <name>InstallDriverWindow</name>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="123"/>
        <source>Select a driver from</source>
        <translation>تعيين مشغل من</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="127"/>
        <source>Local driver</source>
        <translation>ملف التعريف المحلي</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="128"/>
        <source>Local PPD file</source>
        <translation>ملف PPD محلي</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="129"/>
        <source>Search for a driver</source>
        <translation>البحث عن مشغل</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="143"/>
        <location filename="../ui/installdriverwindow.cpp" line="421"/>
        <source>Choose a local driver</source>
        <translation>اختار ملف تعريف محلي</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="146"/>
        <source>Vendor</source>
        <translation>البائع</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="158"/>
        <source>Model</source>
        <translation>النموذج</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="169"/>
        <location filename="../ui/installdriverwindow.cpp" line="238"/>
        <source>Driver</source>
        <translation>المشغل أو برنامج التعريف</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="223"/>
        <source>Vendor and Model</source>
        <translation>البائع أو النموذج</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="225"/>
        <source>Enter a complete vendor and model (Only letters, numbers and whitespaces)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enter a complete vendor and model</source>
        <translation type="vanished">أدخل الإسم الكامل للبائع أو النموذج</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="227"/>
        <source>Search</source>
        <translation>البحث</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="268"/>
        <source>Install Driver</source>
        <translation>تثبيت مشغل</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="403"/>
        <location filename="../ui/installdriverwindow.cpp" line="515"/>
        <source>Reselect</source>
        <translation>إعادة تعيين</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="429"/>
        <location filename="../ui/installdriverwindow.cpp" line="512"/>
        <source>Select a PPD file</source>
        <translation>اختيار ملف PPD</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="438"/>
        <source>Search for printer driver</source>
        <translation>البحث عن مشغل الطابعة</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="494"/>
        <source>(recommended)</source>
        <translation>(موصى به)</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="656"/>
        <source> (recommended)</source>
        <translation>(موصى به)</translation>
    </message>
</context>
<context>
    <name>InstallInterface</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="252"/>
        <source>is invalid</source>
        <translation>غير مقبول</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="281"/>
        <source>Failed to install the driver by calling dbus interface</source>
        <translation>لم يتم تثبيت مشغل بالإتصال بالواجهة dbus</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="326"/>
        <source>Failed to install %1</source>
        <translation>فشل في تثبيت ٪ 1</translation>
    </message>
</context>
<context>
    <name>InstallPrinterWindow</name>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="85"/>
        <location filename="../ui/installprinterwindow.cpp" line="148"/>
        <source>Installing driver...</source>
        <translation>تثبيت المشغل...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="95"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="98"/>
        <location filename="../ui/installprinterwindow.cpp" line="171"/>
        <location filename="../ui/installprinterwindow.cpp" line="188"/>
        <source>View Printer</source>
        <translation>إظهار الطابعة</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="100"/>
        <location filename="../ui/installprinterwindow.cpp" line="173"/>
        <location filename="../ui/installprinterwindow.cpp" line="190"/>
        <source>Print Test Page</source>
        <translation>طبع صفحة مسودة</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="160"/>
        <source>Successfully installed </source>
        <translation>عملية التثبيت تمت بنجاح</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="166"/>
        <location filename="../ui/installprinterwindow.cpp" line="184"/>
        <source>You have successfully added the printer. Print a test page to check if it works properly.</source>
        <translation>إضافة الطابعة تمت بنجاح. إطبع صفحة مسودة للتأكد من إشتغالها</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="178"/>
        <source>Printing test page...</source>
        <translation>طبع صفحة مسودة...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="195"/>
        <source>Did you print the test page successfully?</source>
        <translation>هل طبعت صفحة مسودة بنجاح؟</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="201"/>
        <source>If succeeded, click Yes; if failed, click No</source>
        <translation>في حالة النجاح، انقر فوق &quot;نعم&quot; ؛ في حالة الفشل ، انقر فوق &quot;لا&quot;</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="204"/>
        <source>No</source>
        <translation>نعم</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="206"/>
        <source>Yes</source>
        <translation>لا</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="211"/>
        <location filename="../ui/installprinterwindow.cpp" line="231"/>
        <source>Print failed</source>
        <translation>الطبع لم يتم</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="217"/>
        <source>Click Reinstall to install the printer driver again, or click Troubleshoot to start troubleshooting.</source>
        <translation>انقر فوق &quot;إعادة التثبيت&quot; لتثبيت ملف تعريف الطابعة مرة أخرى ، أو انقر فوق &quot;استكشاف الأخطاء&quot; لبدء استكشاف الأخطاء وإصلاحها.</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="220"/>
        <location filename="../ui/installprinterwindow.cpp" line="241"/>
        <source>Reinstall</source>
        <translation>إعادة تثبيت</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="222"/>
        <location filename="../ui/installprinterwindow.cpp" line="243"/>
        <source>Troubleshoot</source>
        <translation>إستكشاف الأخطاء</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="229"/>
        <source>Installation failed</source>
        <translation>التثبيت لم يتم</translation>
    </message>
</context>
<context>
    <name>JobListView</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="214"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="371"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="215"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="372"/>
        <source>Delete</source>
        <translation>حذف</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="216"/>
        <source>Pause</source>
        <translation>مقاطعة</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="217"/>
        <source>Resume</source>
        <translation>مواصلة</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="218"/>
        <source>Print first</source>
        <translation>الطبع أولا</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="219"/>
        <source>Reprint</source>
        <translation>إعادة الطبع</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="221"/>
        <source>No print jobs</source>
        <translation>لا توجد عمليات طبع</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="365"/>
        <source>Are you sure you want to delete this job?</source>
        <translation>هل أنت متأكد من حذف هذه العملية؟</translation>
    </message>
</context>
<context>
    <name>JobManager</name>
    <message>
        <location filename="../vendor/zjobmanager.cpp" line="279"/>
        <source> not found</source>
        <translation>غير موجود</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1007"/>
        <source>Refresh</source>
        <translation>تحيين</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1011"/>
        <source>All</source>
        <translation>الكل</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1012"/>
        <source>Print Queue</source>
        <translation>قائمة الطبع</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1013"/>
        <source>Completed</source>
        <translation>تمت العملية</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1070"/>
        <source> failed</source>
        <translation>العملية لم تتم</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1091"/>
        <source>%1 jobs</source>
        <translation>%1 عمليات</translation>
    </message>
</context>
<context>
    <name>JobsDataModel</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="631"/>
        <source>Job</source>
        <translation>العملية</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="633"/>
        <source>User</source>
        <translation>المستخدم</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="635"/>
        <source>Document</source>
        <translation>المستند</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="637"/>
        <source>Printer</source>
        <translation>الطابعة</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="639"/>
        <source>Size</source>
        <translation>الأبعاد</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="641"/>
        <source>Time submitted</source>
        <translation>تم إرسال الوقت</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="643"/>
        <source>Status</source>
        <translation>الحالة</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="645"/>
        <source>Action</source>
        <translation>اجراء</translation>
    </message>
</context>
<context>
    <name>PermissionsWindow</name>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="48"/>
        <source>Connect to %1 to find a printer</source>
        <translation>إتصل ب‍ 1% لإيجاد طابعة</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="74"/>
        <source>Username</source>
        <translation>إسم المستخدم</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="76"/>
        <source>Group</source>
        <translation>المجموعة</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="78"/>
        <source>Password</source>
        <translation>كلمة السر</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="92"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="93"/>
        <source>Connect</source>
        <translation>إتصال</translation>
    </message>
</context>
<context>
    <name>PrinterApplication</name>
    <message>
        <location filename="../ui/printerapplication.cpp" line="111"/>
        <source>Print Manager</source>
        <translation>مدير الطباعة</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="112"/>
        <source>Print Manager is a printer management tool, which supports adding and removing printers, managing print jobs and so on.</source>
        <translation>إدارة الطباعة هي أداة إدارة الطابعة ، والتي تدعم إضافة وإزالة الطابعات ، وإدارة مهام الطباعة وما إلى ذلك.</translation>
    </message>
</context>
<context>
    <name>PrinterSearchWindow</name>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="83"/>
        <location filename="../ui/printersearchwindow.cpp" line="86"/>
        <source>Discover printer</source>
        <translation>إكتشاف الطابعة</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="89"/>
        <location filename="../ui/printersearchwindow.cpp" line="92"/>
        <source>Find printer</source>
        <translation>إيجاد الطابعة</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="94"/>
        <location filename="../ui/printersearchwindow.cpp" line="97"/>
        <source>Enter URI</source>
        <translation>إدخال الرابط URI</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="118"/>
        <source>Select a printer</source>
        <translation>تعيين طابعة</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="124"/>
        <source>Refresh</source>
        <translation>تحيين</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="162"/>
        <source>Install Driver</source>
        <translation>تثبيت مشغل</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="190"/>
        <source>Address</source>
        <translation>العنوان</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="192"/>
        <source>Enter an address</source>
        <translation>إدخال عنوان</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="193"/>
        <source>Find</source>
        <translation>إيجاد</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="266"/>
        <source>URI</source>
        <translation>الرابط URI</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="270"/>
        <source>Enter device URI</source>
        <translation>إدخال URI التابع لجهازك</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="281"/>
        <source>Examples:</source>
        <translation>أمثلة:</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="556"/>
        <source>OK</source>
        <translation>جيد</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="644"/>
        <source> (recommended)</source>
        <translation>(موصى به)</translation>
    </message>
</context>
<context>
    <name>PrinterTestJob</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="369"/>
        <source>Check test page printing</source>
        <translation>تأكد من طبع الصفحة المسودة</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="416"/>
        <source>Printing test page...</source>
        <translation>طبع الصفحة المسودة...</translation>
    </message>
</context>
<context>
    <name>PrinterTestPageDialog</name>
    <message>
        <location filename="../ui/printertestpagedialog.cpp" line="63"/>
        <source>OK</source>
        <translation>جيد</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="103"/>
        <source>1 min ago</source>
        <translation>منذ دقيقة</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="106"/>
        <source>%1 mins ago</source>
        <translation>منذ 1% دقيقة</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="110"/>
        <source>1 hr ago</source>
        <translation>منذ ساعة</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="112"/>
        <source>%1 hrs ago</source>
        <translation>منذ 1% ساعات</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="117"/>
        <source>Yesterday</source>
        <translation>الأمس</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="119"/>
        <source>%1 days ago</source>
        <translation>منذ 1% أيام</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="481"/>
        <source>(use %1 protocol)</source>
        <translation>(استخدم ٪ 1 بروتوكول)</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="26"/>
        <source>Select a driver</source>
        <translation>تعيين مشغل</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="32"/>
        <source>Driver</source>
        <translation>المشغل أو برنامج التعريف</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="33"/>
        <source>Next</source>
        <translation>التالي</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="34"/>
        <source>Install Driver</source>
        <translation>تثبيت المشغل</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="36"/>
        <source>Drag a PPD file here 
 or</source>
        <translation>اسحب ملف PPD هنا
أو</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="37"/>
        <source>Select a PPD file</source>
        <translation>اختيار ملف PPD</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="39"/>
        <source>Troubleshoot</source>
        <translation>إستكشاف الأخطاء</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="40"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../vendor/common.cpp" line="149"/>
        <source> not found, please ask the administrator for help</source>
        <translation>غير موجود ، الرجاء طلب المساعدة من المسؤول</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="121"/>
        <source>Direct-attached Device</source>
        <translation>جهاز متصل مباشرة</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="122"/>
        <source>File</source>
        <translation>ملف</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="255"/>
        <source>Black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="256"/>
        <source>Blue</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="257"/>
        <source>Brown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="258"/>
        <source>Cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="259"/>
        <source>Dark-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="260"/>
        <source>Dark gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="261"/>
        <source>Dark-yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="262"/>
        <source>Dark yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="263"/>
        <source>Gold</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="264"/>
        <source>Gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="265"/>
        <source>Green</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="266"/>
        <source>Light-black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="267"/>
        <source>Light black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="268"/>
        <source>Light-cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="269"/>
        <source>Light cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="270"/>
        <source>Light-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="271"/>
        <source>Light gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="272"/>
        <source>Light-magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="273"/>
        <source>Light magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="274"/>
        <source>Magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="275"/>
        <source>Orange</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="276"/>
        <source>Red</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="277"/>
        <source>Silver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="278"/>
        <source>White</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="279"/>
        <source>Yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="280"/>
        <source>Waste</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RenamePrinterWindow</name>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="84"/>
        <source>Rename Printer</source>
        <translation>تغيير إسم الطابعة</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="91"/>
        <source>Name</source>
        <translation>الإسم</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="93"/>
        <source>Location</source>
        <translation>المكان</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="95"/>
        <source>Description</source>
        <translation>الوصف</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="106"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="107"/>
        <source>Confirm</source>
        <translation>تأكيد</translation>
    </message>
</context>
<context>
    <name>ServerSettingsWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="85"/>
        <source>Basic Server Settings</source>
        <translation>تعديلات الخادم الأساسية</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="93"/>
        <source>Publish shared printers connected to this system</source>
        <translation>انشر الطابعات المشتركة المتصلة بهذا النظام</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="94"/>
        <source>Allow printing from the Internet</source>
        <translation>السماح للطباعة من اﻹنترنت</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="96"/>
        <source>Allow remote administration</source>
        <translation>السماح للإدارة عن بعد</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="98"/>
        <source>Save debugging information for troubleshooting</source>
        <translation>حفظ معلومات التصحيح لاستكشاف الأخطاء وإصلاحها</translation>
    </message>
</context>
<context>
    <name>TroubleShootDialog</name>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="109"/>
        <source>Troubleshoot: </source>
        <translation>استكشاف الأخطاء</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="134"/>
        <source>Cancel</source>
        <translation>إلغاء</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="165"/>
        <source>OK</source>
        <translation>جيد</translation>
    </message>
</context>
</TS>