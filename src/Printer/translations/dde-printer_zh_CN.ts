<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_CN" version="2.1">
<context>
    <name>AddPrinterTask</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="547"/>
        <source>URI and driver do not match.</source>
        <translation>URI和驱动不匹配。</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="549"/>
        <source>Install hplip first and restart the app to install the driver again.</source>
        <translation>请安装hplip后重新添加。</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="557"/>
        <source>Please select an hplip driver and try again.</source>
        <translation>请选择其他hplip驱动后重试。</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="570"/>
        <source>URI can&apos;t be empty</source>
        <translation>URI不能为空</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="577"/>
        <source> not found</source>
        <translation>未找到</translation>
    </message>
</context>
<context>
    <name>CheckAttributes</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="306"/>
        <source>Check printer settings</source>
        <translation>检查打印机设置</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="315"/>
        <source>Checking printer settings...</source>
        <translation>检查打印机设置中...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="318"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="326"/>
        <source>Failed to get printer attributes, error: </source>
        <translation>获取打印机属性失败。错误原因：</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="334"/>
        <source>%1 is disabled</source>
        <translation>%1未启用</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="343"/>
        <source>is not accepting jobs</source>
        <translation>不接收任务</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="348"/>
        <source>Printer settings are ok</source>
        <translation>打印机设置正常</translation>
    </message>
</context>
<context>
    <name>CheckConnected</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="211"/>
        <source>Check printer connection</source>
        <translation>检查打印机连接状态</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="223"/>
        <source>Checking printer connection...</source>
        <translation>检查打印机连接状态中...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="226"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="263"/>
        <source>Cannot connect to the printer, error: %1</source>
        <translation>连接打印机失败，错误原因：%1</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="271"/>
        <source> is not connected, URI: </source>
        <translation>未连接，URI：</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="280"/>
        <source>%1 does not exist</source>
        <translation>%1 目录不存在</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="289"/>
        <source>Cannot connect to the printer</source>
        <translation>连接打印机失败</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="295"/>
        <source>The connection is valid</source>
        <translation>打印机连接状态正常</translation>
    </message>
</context>
<context>
    <name>CheckCupsServer</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="103"/>
        <source>Check CUPS server</source>
        <translation>检查CUPS服务是否开启</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="111"/>
        <source>Checking CUPS server...</source>
        <translation>检查CUPS服务中...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="117"/>
        <source>CUPS server is invalid</source>
        <translation>CUPS服务异常</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="122"/>
        <source>CUPS server is valid</source>
        <translation>CUPS服务正常</translation>
    </message>
</context>
<context>
    <name>CheckDriver</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="140"/>
        <source>Checking driver...</source>
        <translation>检查驱动文件中...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="143"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="151"/>
        <source>PPD file %1 not found</source>
        <translation>PPD文件 %1 未找到</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="161"/>
        <source>The driver is damaged</source>
        <translation>驱动文件已损坏</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="177"/>
        <source>Driver filter %1 not found</source>
        <translation>驱动Filter %1 未找到</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="189"/>
        <source>%1 is not installed, cannot print now</source>
        <translation>%1 未安装，打印异常</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="195"/>
        <source>Driver is valid</source>
        <translation>驱动文件完好</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="201"/>
        <source>Check driver</source>
        <translation>检查驱动</translation>
    </message>
</context>
<context>
    <name>CupsMonitor</name>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="74"/>
        <source>Queuing</source>
        <translation>排队中</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="75"/>
        <source>Paused</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="76"/>
        <source>Printing</source>
        <translation>打印中</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="77"/>
        <source>Stopped</source>
        <translation>已停止</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="78"/>
        <source>Canceled</source>
        <translation>已取消</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="79"/>
        <source>Error</source>
        <translation>打印异常</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="80"/>
        <source>Completed</source>
        <translation>已完成</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="293"/>
        <source>%1 timed out, reason: %2</source>
        <translation>%1打印超时，原因：%2</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="307"/>
        <source>%1 printed successfully, please take away the paper in time!</source>
        <translation>%1打印完成，请及时取走纸质文件！</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="309"/>
        <source>%1 %2, reason: %3</source>
        <translation>%1%2，原因：%3</translation>
    </message>
</context>
<context>
    <name>DDestination</name>
    <message>
        <location filename="../util/ddestination.cpp" line="148"/>
        <location filename="../util/ddestination.cpp" line="202"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="209"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="212"/>
        <source>Printing</source>
        <translation>打印中</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="215"/>
        <source>Disabled</source>
        <translation>已停用</translation>
    </message>
</context>
<context>
    <name>DPrinterSupplyShowDlg</name>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="75"/>
        <source>Ink/Toner Status</source>
        <translation>耗材余量</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="81"/>
        <source>Unknown amount</source>
        <translation>油墨量未知</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="88"/>
        <source>Unable to get the remaining amount</source>
        <translation>未获取到油墨量数据</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="113"/>
        <source>The amounts are estimated, last updated at %1:%2</source>
        <translation>所有余量为估计值 ，更新时间为今天%1:%2</translation>
    </message>
    <message>
        <source>The amounts are estimated, last updated at%1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="122"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="221"/>
        <source>Unknown</source>
        <translation>余量未知</translation>
    </message>
</context>
<context>
    <name>DPrinterTanslator</name>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="33"/>
        <source>Color</source>
        <translation>彩色</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="34"/>
        <location filename="../util/dprintertanslator.cpp" line="35"/>
        <source>Grayscale</source>
        <translation>灰度</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="38"/>
        <location filename="../util/dprintertanslator.cpp" line="69"/>
        <location filename="../util/dprintertanslator.cpp" line="86"/>
        <location filename="../util/dprintertanslator.cpp" line="107"/>
        <location filename="../util/dprintertanslator.cpp" line="126"/>
        <location filename="../util/dprintertanslator.cpp" line="135"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="39"/>
        <location filename="../util/dprintertanslator.cpp" line="49"/>
        <source>Draft</source>
        <translation>草稿</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="40"/>
        <location filename="../util/dprintertanslator.cpp" line="120"/>
        <source>Normal</source>
        <translation>从前向后</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="43"/>
        <location filename="../util/dprintertanslator.cpp" line="44"/>
        <location filename="../util/dprintertanslator.cpp" line="47"/>
        <source>Print Quality</source>
        <translation>打印质量</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="55"/>
        <location filename="../util/dprintertanslator.cpp" line="56"/>
        <source>Auto-Select</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="57"/>
        <location filename="../util/dprintertanslator.cpp" line="58"/>
        <source>Manual Feeder</source>
        <translation>多功能托盘</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="59"/>
        <location filename="../util/dprintertanslator.cpp" line="73"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="48"/>
        <location filename="../util/dprintertanslator.cpp" line="60"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="52"/>
        <location filename="../util/dprintertanslator.cpp" line="53"/>
        <location filename="../util/dprintertanslator.cpp" line="54"/>
        <source>Paper Source</source>
        <translation>纸张来源</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="61"/>
        <source>Drawer 1</source>
        <translation>纸盒1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="62"/>
        <source>Drawer 2</source>
        <translation>纸盒2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="63"/>
        <source>Drawer 3</source>
        <translation>纸盒3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="64"/>
        <source>Drawer 4</source>
        <translation>纸盒4</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="65"/>
        <source>Drawer 5</source>
        <translation>纸盒5</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="66"/>
        <source>Envelope Feeder</source>
        <translation>信封输送器</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="67"/>
        <source>Tray1</source>
        <translation>托盘1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="68"/>
        <source>Unknown</source>
        <translation>未定义的</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="72"/>
        <source>MediaType</source>
        <translation>纸张类型</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="74"/>
        <source>Plain Paper</source>
        <translation>普通纸</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="75"/>
        <source>Recycled Paper</source>
        <translation>再生纸</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="76"/>
        <source>Color Paper</source>
        <translation>彩色纸</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="77"/>
        <source>Bond Paper</source>
        <translation>铜版纸</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="78"/>
        <source>Heavy Paper 1</source>
        <translation>重磅纸1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="79"/>
        <source>Heavy Paper 2</source>
        <translation>重磅纸2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="80"/>
        <source>Heavy Paper 3</source>
        <translation>重磅纸3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="81"/>
        <location filename="../util/dprintertanslator.cpp" line="82"/>
        <source>OHP</source>
        <translation>胶片</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="83"/>
        <source>Labels</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="84"/>
        <source>Envelope</source>
        <translation>信封</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="85"/>
        <source>Photo Paper</source>
        <translation>照片纸</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="89"/>
        <location filename="../util/dprintertanslator.cpp" line="90"/>
        <location filename="../util/dprintertanslator.cpp" line="91"/>
        <source>PageSize</source>
        <translation>纸张大小</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="92"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="95"/>
        <location filename="../util/dprintertanslator.cpp" line="96"/>
        <location filename="../util/dprintertanslator.cpp" line="97"/>
        <source>Duplex</source>
        <translation>双面打印</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="98"/>
        <source>DuplexTumble</source>
        <translation>双面，短边</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="99"/>
        <source>DuplexNoTumble</source>
        <translation>双面，长边</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="100"/>
        <source>ON (Long-edged Binding)</source>
        <translation>双面，长边</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="101"/>
        <source>ON (Short-edged Binding)</source>
        <translation>双面，短边</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="102"/>
        <location filename="../util/dprintertanslator.cpp" line="103"/>
        <source>OFF</source>
        <translation>单面</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="106"/>
        <location filename="../util/dprintertanslator.cpp" line="110"/>
        <source>Binding Edge</source>
        <translation>装订边</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="124"/>
        <location filename="../util/dprintertanslator.cpp" line="125"/>
        <source>Staple Location</source>
        <translation>装订位置</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="136"/>
        <location filename="../util/dprintertanslator.cpp" line="137"/>
        <source>Resolution</source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="108"/>
        <source>Left</source>
        <translation>左侧</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="109"/>
        <source>Top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="113"/>
        <source>Portrait (no rotation)</source>
        <translation>纵向（无旋转）</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="114"/>
        <source>Landscape (90 degrees)</source>
        <translation>横向（90 度）</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="115"/>
        <source>Reverse landscape (270 degrees)</source>
        <translation>反横向（270 度）</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="116"/>
        <source>Reverse portrait (180 degrees)</source>
        <translation>反纵向（180 度）</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="117"/>
        <source>Auto Rotation</source>
        <translation>自动旋转</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="121"/>
        <source>Reverse</source>
        <translation>从后往前</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="127"/>
        <source>Bind</source>
        <translation>装订</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="128"/>
        <source>Bind (none)</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="129"/>
        <source>Bind (bottom)</source>
        <translation>装订（下）</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="130"/>
        <source>Bind (left)</source>
        <translation>装订（左）</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="131"/>
        <source>Bind (right)</source>
        <translation>装订（右）</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="132"/>
        <source>Bind (top)</source>
        <translation>装订（上）</translation>
    </message>
</context>
<context>
    <name>DPrintersShowWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="81"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="87"/>
        <source>Printers</source>
        <translation>打印机设备</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="91"/>
        <source>Add printer</source>
        <translation>添加打印机</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="94"/>
        <source>Delete printer</source>
        <translation>删除打印机</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="115"/>
        <source>Shared</source>
        <translation>共享</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="118"/>
        <source>Enabled</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="121"/>
        <source>Accept jobs</source>
        <translation>接受任务</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="125"/>
        <source>Set as default</source>
        <translation>设为默认</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="136"/>
        <source>No Printers</source>
        <translation>无打印设备</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="160"/>
        <source>Location:</source>
        <translation>位置：</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="164"/>
        <source>Model:</source>
        <translation>型号：</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="167"/>
        <source>Status:</source>
        <translation>状态：</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="195"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="205"/>
        <source>Print Queue</source>
        <translation>打印队列</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="215"/>
        <source>Print Test Page</source>
        <translation>打印测试页</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="237"/>
        <source>Supplies</source>
        <translation>耗材</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="262"/>
        <source>No printer configured</source>
        <translation>未配置打印机</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="267"/>
        <source>Click + to add printers</source>
        <translation>请点击添加按钮（+），添加打印机</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="330"/>
        <source>As print jobs are in process, you cannot rename the printer now, please try later</source>
        <translation>有正在进行的打印任务，无法重命名，请稍后再试</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="334"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="382"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="601"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="360"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="362"/>
        <source>Printing</source>
        <translation>打印中</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="364"/>
        <source>Disabled</source>
        <translation>未启用</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="379"/>
        <source>CUPS server is not running, and can’t manage printers.</source>
        <translation>CUPS服务器未运行，不能管理打印机。</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="559"/>
        <source>Are you sure you want to delete the printer &quot;%1&quot; ?</source>
        <translation>您确定要删除打印机“%1”吗？</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="565"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="597"/>
        <source>The name already exists</source>
        <translation>此名称已存在</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="616"/>
        <source>You will not be able to reprint the completed jobs if continue. Are you sure you want to rename the printer?</source>
        <translation>重命名会导致已完成的任务不能重复打印，您确定要重命名吗？</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="621"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="679"/>
        <source>The driver is damaged, please install it again.</source>
        <translation>驱动文件已损坏，请重新安装驱动。</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="682"/>
        <source>Install Driver</source>
        <translation>安装驱动</translation>
    </message>
</context>
<context>
    <name>DPropertySetDlg</name>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="175"/>
        <source>Print Properties</source>
        <translation>打印属性</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="176"/>
        <source>Driver</source>
        <translation>驱动</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="177"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="178"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="179"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="180"/>
        <source>Color Mode</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="181"/>
        <source>Orientation</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="182"/>
        <source>Page Order</source>
        <translation>打印顺序</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="603"/>
        <source>Options conflict!</source>
        <translation>选项冲突!</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="606"/>
        <source>Please resolve the conflict first, and then save the changes.</source>
        <translation>只有在冲突解决后才能应用所做修改!</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="625"/>
        <source>Conflict:</source>
        <translation>冲突项：</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="636"/>
        <location filename="../ui/dpropertysetdlg.cpp" line="1123"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="1122"/>
        <source>Invalid URI</source>
        <translation>URI格式不正确</translation>
    </message>
</context>
<context>
    <name>DriverManager</name>
    <message>
        <location filename="../vendor/zdrivermanager.cpp" line="791"/>
        <source>EveryWhere driver</source>
        <translation>EveryWhere 无驱方案</translation>
    </message>
</context>
<context>
    <name>InstallDriver</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="374"/>
        <source>Failed to find the driver solution: %1, error: %2</source>
        <translation>查找驱动解决方案：%1失败，错误码：%2</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="385"/>
        <source>The solution is invalid</source>
        <translation>服务器的驱动数据不对</translation>
    </message>
</context>
<context>
    <name>InstallDriverWindow</name>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="123"/>
        <source>Select a driver from</source>
        <translation>选择驱动来源</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="127"/>
        <source>Local driver</source>
        <translation>本地驱动</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="128"/>
        <source>Local PPD file</source>
        <translation>本地PPD文件</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="129"/>
        <source>Search for a driver</source>
        <translation>搜索打印机驱动</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="143"/>
        <location filename="../ui/installdriverwindow.cpp" line="421"/>
        <source>Choose a local driver</source>
        <translation>选择本地驱动</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="146"/>
        <source>Vendor</source>
        <translation>厂商</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="158"/>
        <source>Model</source>
        <translation>型号</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="169"/>
        <location filename="../ui/installdriverwindow.cpp" line="238"/>
        <source>Driver</source>
        <translation>驱动</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="223"/>
        <source>Vendor and Model</source>
        <translation>厂商和型号</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="225"/>
        <source>Enter a complete vendor and model (Only letters, numbers and whitespaces)</source>
        <translation>请输入完整的厂商和型号（仅限字母、数字及空格）</translation>
    </message>
    <message>
        <source>Enter a complete vendor and model</source>
        <translation type="vanished">请输入完整的厂商和型号</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="227"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="268"/>
        <source>Install Driver</source>
        <translation>安装驱动</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="403"/>
        <location filename="../ui/installdriverwindow.cpp" line="515"/>
        <source>Reselect</source>
        <translation>重新选择</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="429"/>
        <location filename="../ui/installdriverwindow.cpp" line="512"/>
        <source>Select a PPD file</source>
        <translation>选择PPD文件</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="438"/>
        <source>Search for printer driver</source>
        <translation>搜索打印机驱动</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="494"/>
        <source>(recommended)</source>
        <translation>（推荐）</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="656"/>
        <source> (recommended)</source>
        <translation>（推荐）</translation>
    </message>
</context>
<context>
    <name>InstallInterface</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="252"/>
        <source>is invalid</source>
        <translation>无法安装</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="281"/>
        <source>Failed to install the driver by calling dbus interface</source>
        <translation>通过dbus安装驱动失败</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="326"/>
        <source>Failed to install %1</source>
        <translation>安装%1失败</translation>
    </message>
</context>
<context>
    <name>InstallPrinterWindow</name>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="85"/>
        <location filename="../ui/installprinterwindow.cpp" line="148"/>
        <source>Installing driver...</source>
        <translation>正在安装驱动...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="95"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="98"/>
        <location filename="../ui/installprinterwindow.cpp" line="171"/>
        <location filename="../ui/installprinterwindow.cpp" line="188"/>
        <source>View Printer</source>
        <translation>查看打印机</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="100"/>
        <location filename="../ui/installprinterwindow.cpp" line="173"/>
        <location filename="../ui/installprinterwindow.cpp" line="190"/>
        <source>Print Test Page</source>
        <translation>打印测试页</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="160"/>
        <source>Successfully installed </source>
        <translation>安装成功</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="166"/>
        <location filename="../ui/installprinterwindow.cpp" line="184"/>
        <source>You have successfully added the printer. Print a test page to check if it works properly.</source>
        <translation>添加打印机成功。请打印测试页测试打印机是否能够正常打印。</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="178"/>
        <source>Printing test page...</source>
        <translation>测试页打印中...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="195"/>
        <source>Did you print the test page successfully?</source>
        <translation>测试页是否打印成功?</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="201"/>
        <source>If succeeded, click Yes; if failed, click No</source>
        <translation>如果打印成功，请选择是；如果打印失败，请选择否</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="204"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="206"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="211"/>
        <location filename="../ui/installprinterwindow.cpp" line="231"/>
        <source>Print failed</source>
        <translation>打印失败</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="217"/>
        <source>Click Reinstall to install the printer driver again, or click Troubleshoot to start troubleshooting.</source>
        <translation>选择重新安装将再次安装打印机驱动，选择故障排查将进行打印机故障诊断</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="220"/>
        <location filename="../ui/installprinterwindow.cpp" line="241"/>
        <source>Reinstall</source>
        <translation>重新安装</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="222"/>
        <location filename="../ui/installprinterwindow.cpp" line="243"/>
        <source>Troubleshoot</source>
        <translation>故障排查</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="229"/>
        <source>Installation failed</source>
        <translation>安装失败</translation>
    </message>
</context>
<context>
    <name>JobListView</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="214"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="371"/>
        <source>Cancel</source>
        <translation>取消打印</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="215"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="372"/>
        <source>Delete</source>
        <translation>删除任务</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="216"/>
        <source>Pause</source>
        <translation>暂停打印</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="217"/>
        <source>Resume</source>
        <translation>恢复打印</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="218"/>
        <source>Print first</source>
        <translation>优先打印</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="219"/>
        <source>Reprint</source>
        <translation>重新打印</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="221"/>
        <source>No print jobs</source>
        <translation>暂无打印任务</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="365"/>
        <source>Are you sure you want to delete this job?</source>
        <translation>您确定要删除此打印任务吗？</translation>
    </message>
</context>
<context>
    <name>JobManager</name>
    <message>
        <location filename="../vendor/zjobmanager.cpp" line="279"/>
        <source> not found</source>
        <translation>未找到</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1007"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1011"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1012"/>
        <source>Print Queue</source>
        <translation>打印队列</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1013"/>
        <source>Completed</source>
        <translation>已完成</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1070"/>
        <source> failed</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1091"/>
        <source>%1 jobs</source>
        <translation>%1项任务</translation>
    </message>
</context>
<context>
    <name>JobsDataModel</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="631"/>
        <source>Job</source>
        <translation>任务</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="633"/>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="635"/>
        <source>Document</source>
        <translation>文档</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="637"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="639"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="641"/>
        <source>Time submitted</source>
        <translation>提交时间</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="643"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="645"/>
        <source>Action</source>
        <translation>操作</translation>
    </message>
</context>
<context>
    <name>PermissionsWindow</name>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="48"/>
        <source>Connect to %1 to find a printer</source>
        <translation>查找打印机需要登录%1</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="74"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="76"/>
        <source>Group</source>
        <translation>群组</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="78"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="92"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="93"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
</context>
<context>
    <name>PrinterApplication</name>
    <message>
        <location filename="../ui/printerapplication.cpp" line="111"/>
        <source>Print Manager</source>
        <translation>打印管理器</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="112"/>
        <source>Print Manager is a printer management tool, which supports adding and removing printers, managing print jobs and so on.</source>
        <translation>打印管理器是一款基于CUPS的打印机管理工具，支持打印机管理，打印任务管理等。</translation>
    </message>
</context>
<context>
    <name>PrinterSearchWindow</name>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="83"/>
        <location filename="../ui/printersearchwindow.cpp" line="86"/>
        <source>Discover printer</source>
        <translation>自动查找</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="89"/>
        <location filename="../ui/printersearchwindow.cpp" line="92"/>
        <source>Find printer</source>
        <translation>手动查找</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="94"/>
        <location filename="../ui/printersearchwindow.cpp" line="97"/>
        <source>Enter URI</source>
        <translation>URI查找</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="118"/>
        <source>Select a printer</source>
        <translation>选择打印机</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="124"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="162"/>
        <source>Install Driver</source>
        <translation>安装驱动</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="190"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="192"/>
        <source>Enter an address</source>
        <translation>请输入地址</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="193"/>
        <source>Find</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="266"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="270"/>
        <source>Enter device URI</source>
        <translation>请输入URI</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="281"/>
        <source>Examples:</source>
        <translation>例如：</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="556"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="644"/>
        <source> (recommended)</source>
        <translation>（推荐）</translation>
    </message>
</context>
<context>
    <name>PrinterTestJob</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="369"/>
        <source>Check test page printing</source>
        <translation>检查测试页打印情况</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="416"/>
        <source>Printing test page...</source>
        <translation>测试页打印中...</translation>
    </message>
</context>
<context>
    <name>PrinterTestPageDialog</name>
    <message>
        <location filename="../ui/printertestpagedialog.cpp" line="63"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="103"/>
        <source>1 min ago</source>
        <translation>1分钟以前</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="106"/>
        <source>%1 mins ago</source>
        <translation>%1分钟以前</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="110"/>
        <source>1 hr ago</source>
        <translation>1小时以前</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="112"/>
        <source>%1 hrs ago</source>
        <translation>%1小时以前</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="117"/>
        <source>Yesterday</source>
        <translation>昨天</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="119"/>
        <source>%1 days ago</source>
        <translation>%1天以前</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="481"/>
        <source>(use %1 protocol)</source>
        <translation>（使用%1协议）</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="26"/>
        <source>Select a driver</source>
        <translation>手动选择驱动方案</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="32"/>
        <source>Driver</source>
        <translation>驱动</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="33"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="34"/>
        <source>Install Driver</source>
        <translation>安装驱动</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="36"/>
        <source>Drag a PPD file here 
 or</source>
        <translation>请拖放PPD文件到此处
或</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="37"/>
        <source>Select a PPD file</source>
        <translation>选择一个PPD文件</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="39"/>
        <source>Troubleshoot</source>
        <translation>故障排查</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="40"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../vendor/common.cpp" line="149"/>
        <source> not found, please ask the administrator for help</source>
        <translation>无法解析，请联系管理员</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="121"/>
        <source>Direct-attached Device</source>
        <translation>直连设备</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="122"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="255"/>
        <source>Black</source>
        <translation>黑色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="256"/>
        <source>Blue</source>
        <translation>蓝色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="257"/>
        <source>Brown</source>
        <translation>褐色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="258"/>
        <source>Cyan</source>
        <translation>青色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="259"/>
        <source>Dark-gray</source>
        <translation>深灰色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="260"/>
        <source>Dark gray</source>
        <translation>深灰色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="261"/>
        <source>Dark-yellow</source>
        <translation>深黄色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="262"/>
        <source>Dark yellow</source>
        <translation>深黄色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="263"/>
        <source>Gold</source>
        <translation>金色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="264"/>
        <source>Gray</source>
        <translation>灰色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="265"/>
        <source>Green</source>
        <translation>绿色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="266"/>
        <source>Light-black</source>
        <translation>浅黑色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="267"/>
        <source>Light black</source>
        <translation>浅黑色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="268"/>
        <source>Light-cyan</source>
        <translation>浅青色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="269"/>
        <source>Light cyan</source>
        <translation>浅青色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="270"/>
        <source>Light-gray</source>
        <translation>浅灰色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="271"/>
        <source>Light gray</source>
        <translation>浅灰色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="272"/>
        <source>Light-magenta</source>
        <translation>浅洋红色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="273"/>
        <source>Light magenta</source>
        <translation>浅洋红色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="274"/>
        <source>Magenta</source>
        <translation>洋红色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="275"/>
        <source>Orange</source>
        <translation>橙色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="276"/>
        <source>Red</source>
        <translation>红色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="277"/>
        <source>Silver</source>
        <translation>银色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="278"/>
        <source>White</source>
        <translation>白色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="279"/>
        <source>Yellow</source>
        <translation>黄色</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="280"/>
        <source>Waste</source>
        <translation>废粉</translation>
    </message>
</context>
<context>
    <name>RenamePrinterWindow</name>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="84"/>
        <source>Rename Printer</source>
        <translation>重命名打印机</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="91"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="93"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="95"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="106"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="107"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>ServerSettingsWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="85"/>
        <source>Basic Server Settings</source>
        <translation>基本服务器设置</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="93"/>
        <source>Publish shared printers connected to this system</source>
        <translation>发布连接到这个系统的共享打印机</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="94"/>
        <source>Allow printing from the Internet</source>
        <translation>允许从互联网打印</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="96"/>
        <source>Allow remote administration</source>
        <translation>允许远程管理</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="98"/>
        <source>Save debugging information for troubleshooting</source>
        <translation>保留调试信息用于故障排除</translation>
    </message>
</context>
<context>
    <name>TroubleShootDialog</name>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="109"/>
        <source>Troubleshoot: </source>
        <translation>故障排查：</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="134"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="165"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
</TS>