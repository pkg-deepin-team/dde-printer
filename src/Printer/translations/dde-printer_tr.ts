<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr" version="2.1">
<context>
    <name>AddPrinterTask</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="547"/>
        <source>URI and driver do not match.</source>
        <translation>URI ve sürücü eşleşmiyor.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="549"/>
        <source>Install hplip first and restart the app to install the driver again.</source>
        <translation>Önce hplip&apos;i kurun ve sürücüyü tekrar kurmakiçin uygulamayı yeniden başlatın.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="557"/>
        <source>Please select an hplip driver and try again.</source>
        <translation>Lütfen bir hplip sürücüsü seçin ve tekrar deneyin.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="570"/>
        <source>URI can&apos;t be empty</source>
        <translation>URI boş olamaz</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="577"/>
        <source> not found</source>
        <translation>bulunamadı</translation>
    </message>
</context>
<context>
    <name>CheckAttributes</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="306"/>
        <source>Check printer settings</source>
        <translation>Yazıcı ayarlarını kontrol et</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="315"/>
        <source>Checking printer settings...</source>
        <translation>Yazıcı ayarları kontrol ediliyor...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="318"/>
        <source>Success</source>
        <translation>Başarılı</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="326"/>
        <source>Failed to get printer attributes, error: </source>
        <translation>Yazıcı özellikleri alınamadı, hata:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="334"/>
        <source>%1 is disabled</source>
        <translation>%1 devre dışı</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="343"/>
        <source>is not accepting jobs</source>
        <translation>iş kabul etmiyor</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="348"/>
        <source>Printer settings are ok</source>
        <translation>Yazıcı ayarları tamam</translation>
    </message>
</context>
<context>
    <name>CheckConnected</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="211"/>
        <source>Check printer connection</source>
        <translation>Yazıcı bağlantısını kontrol et</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="223"/>
        <source>Checking printer connection...</source>
        <translation>Yazıcı bağlantısı kontrol ediliyor...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="226"/>
        <source>Success</source>
        <translation>Başarılı</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="263"/>
        <source>Cannot connect to the printer, error: %1</source>
        <translation>Yazıcıya bağlanılamıyor, hata: %1</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="271"/>
        <source> is not connected, URI: </source>
        <translation>bağlı değil, URI:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="280"/>
        <source>%1 does not exist</source>
        <translation>%1 mevcut değil</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="289"/>
        <source>Cannot connect to the printer</source>
        <translation>Yazıcıya bağlanılamıyor</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="295"/>
        <source>The connection is valid</source>
        <translation>Bağlantı geçerlidir</translation>
    </message>
</context>
<context>
    <name>CheckCupsServer</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="103"/>
        <source>Check CUPS server</source>
        <translation>CUPS sunucusunu kontrol et</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="111"/>
        <source>Checking CUPS server...</source>
        <translation>CUPS sunucusu kontrol ediliyor...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="117"/>
        <source>CUPS server is invalid</source>
        <translation>CUPS sunucusu geçersiz</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="122"/>
        <source>CUPS server is valid</source>
        <translation>CUPS sunucusu geçerli</translation>
    </message>
</context>
<context>
    <name>CheckDriver</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="140"/>
        <source>Checking driver...</source>
        <translation>Sürücü kontrol ediliyor...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="143"/>
        <source>Success</source>
        <translation>Başarılı</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="151"/>
        <source>PPD file %1 not found</source>
        <translation>%1 PPD dosyası bulunamadı</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="161"/>
        <source>The driver is damaged</source>
        <translation>Sürücü hasarlı</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="177"/>
        <source>Driver filter %1 not found</source>
        <translation>%1 sürücü filtresi bulunamadı</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="189"/>
        <source>%1 is not installed, cannot print now</source>
        <translation>%1 yüklü değil, şimdi yazdıramıyor</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="195"/>
        <source>Driver is valid</source>
        <translation>Sürücü geçerli</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="201"/>
        <source>Check driver</source>
        <translation>Sürücüyü kontrol et</translation>
    </message>
</context>
<context>
    <name>CupsMonitor</name>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="74"/>
        <source>Queuing</source>
        <translation>Kuyruk</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="75"/>
        <source>Paused</source>
        <translation>Duraklatıldı</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="76"/>
        <source>Printing</source>
        <translation>Yazdırılıyor</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="77"/>
        <source>Stopped</source>
        <translation>Durduruldu</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="78"/>
        <source>Canceled</source>
        <translation>İptal edildi</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="79"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="80"/>
        <source>Completed</source>
        <translation>Tamamlandı</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="293"/>
        <source>%1 timed out, reason: %2</source>
        <translation>%1 zaman aşımı, nedeni: %2</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="307"/>
        <source>%1 printed successfully, please take away the paper in time!</source>
        <translation>%1 başarıyla yazdırıldı, lütfen kağıtları zamanında alın!</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="309"/>
        <source>%1 %2, reason: %3</source>
        <translation>%1 %2, nedeni: %3</translation>
    </message>
</context>
<context>
    <name>DDestination</name>
    <message>
        <location filename="../util/ddestination.cpp" line="148"/>
        <location filename="../util/ddestination.cpp" line="202"/>
        <source>Unknown</source>
        <translation>Bilinmiyor</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="209"/>
        <source>Idle</source>
        <translation>Boş</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="212"/>
        <source>Printing</source>
        <translation>Yazdırılıyor</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="215"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
</context>
<context>
    <name>DPrinterSupplyShowDlg</name>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="75"/>
        <source>Ink/Toner Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="81"/>
        <source>Unknown amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="88"/>
        <source>Unable to get the remaining amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="113"/>
        <source>The amounts are estimated, last updated at %1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The amounts are estimated, last updated at%1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="122"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="221"/>
        <source>Unknown</source>
        <translation>Bilinmiyor</translation>
    </message>
</context>
<context>
    <name>DPrinterTanslator</name>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="33"/>
        <source>Color</source>
        <translation>Renk</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="34"/>
        <location filename="../util/dprintertanslator.cpp" line="35"/>
        <source>Grayscale</source>
        <translation>Gri tonlama</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="38"/>
        <location filename="../util/dprintertanslator.cpp" line="69"/>
        <location filename="../util/dprintertanslator.cpp" line="86"/>
        <location filename="../util/dprintertanslator.cpp" line="107"/>
        <location filename="../util/dprintertanslator.cpp" line="126"/>
        <location filename="../util/dprintertanslator.cpp" line="135"/>
        <source>None</source>
        <translation>Yok</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="39"/>
        <location filename="../util/dprintertanslator.cpp" line="49"/>
        <source>Draft</source>
        <translation>Taslak</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="40"/>
        <location filename="../util/dprintertanslator.cpp" line="120"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="43"/>
        <location filename="../util/dprintertanslator.cpp" line="44"/>
        <location filename="../util/dprintertanslator.cpp" line="47"/>
        <source>Print Quality</source>
        <translation>Baskı Kalitesi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="55"/>
        <location filename="../util/dprintertanslator.cpp" line="56"/>
        <source>Auto-Select</source>
        <translation>Otomatik-Seçim</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="57"/>
        <location filename="../util/dprintertanslator.cpp" line="58"/>
        <source>Manual Feeder</source>
        <translation>Elle Besleme</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="59"/>
        <location filename="../util/dprintertanslator.cpp" line="73"/>
        <source>Auto</source>
        <translation>Otomatik</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="48"/>
        <location filename="../util/dprintertanslator.cpp" line="60"/>
        <source>Manual</source>
        <translation>Elle</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="52"/>
        <location filename="../util/dprintertanslator.cpp" line="53"/>
        <location filename="../util/dprintertanslator.cpp" line="54"/>
        <source>Paper Source</source>
        <translation>Kağıt Kaynağı</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="61"/>
        <source>Drawer 1</source>
        <translation>Çekmece 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="62"/>
        <source>Drawer 2</source>
        <translation>Çekmece 2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="63"/>
        <source>Drawer 3</source>
        <translation>Çekmece 3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="64"/>
        <source>Drawer 4</source>
        <translation>Çekmece 4</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="65"/>
        <source>Drawer 5</source>
        <translation>Çekmece 5</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="66"/>
        <source>Envelope Feeder</source>
        <translation>Zarf Besleyici</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="67"/>
        <source>Tray1</source>
        <translation>Tepsi1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="68"/>
        <source>Unknown</source>
        <translation>Bilinmiyor</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="72"/>
        <source>MediaType</source>
        <translation>OrtamTürü</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="74"/>
        <source>Plain Paper</source>
        <translation>Düz Kağıt</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="75"/>
        <source>Recycled Paper</source>
        <translation>Geri Dönüştürülmüş Kağıt</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="76"/>
        <source>Color Paper</source>
        <translation>Renkli Kağıt</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="77"/>
        <source>Bond Paper</source>
        <translation>Bond Kağıt</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="78"/>
        <source>Heavy Paper 1</source>
        <translation>Ağır Kağıt 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="79"/>
        <source>Heavy Paper 2</source>
        <translation>Ağır Kağıt 2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="80"/>
        <source>Heavy Paper 3</source>
        <translation>Ağır Kağıt 3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="81"/>
        <location filename="../util/dprintertanslator.cpp" line="82"/>
        <source>OHP</source>
        <translation>OHP</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="83"/>
        <source>Labels</source>
        <translation>Etiketler</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="84"/>
        <source>Envelope</source>
        <translation>Zarf</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="85"/>
        <source>Photo Paper</source>
        <translation>Fotoğraf Kağıdı</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="89"/>
        <location filename="../util/dprintertanslator.cpp" line="90"/>
        <location filename="../util/dprintertanslator.cpp" line="91"/>
        <source>PageSize</source>
        <translation>SayfaBoyutu</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="92"/>
        <source>Custom</source>
        <translation>Özel</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="95"/>
        <location filename="../util/dprintertanslator.cpp" line="96"/>
        <location filename="../util/dprintertanslator.cpp" line="97"/>
        <source>Duplex</source>
        <translation>Çift taraflı</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="98"/>
        <source>DuplexTumble</source>
        <translation>Çift taraflı uzun kenar</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="99"/>
        <source>DuplexNoTumble</source>
        <translation>Tek taraflı kısa kenar</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="100"/>
        <source>ON (Long-edged Binding)</source>
        <translation>AÇIK (Uzun Kenarlı Ciltleme)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="101"/>
        <source>ON (Short-edged Binding)</source>
        <translation>AÇIK (Kısa kenarlı Ciltleme)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="102"/>
        <location filename="../util/dprintertanslator.cpp" line="103"/>
        <source>OFF</source>
        <translation>KAPALI</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="106"/>
        <location filename="../util/dprintertanslator.cpp" line="110"/>
        <source>Binding Edge</source>
        <translation>Cilt Kenarı</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="124"/>
        <location filename="../util/dprintertanslator.cpp" line="125"/>
        <source>Staple Location</source>
        <translation>Zımba Bölgesi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="136"/>
        <location filename="../util/dprintertanslator.cpp" line="137"/>
        <source>Resolution</source>
        <translation>Çözünürlük</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="108"/>
        <source>Left</source>
        <translation>Sol</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="109"/>
        <source>Top</source>
        <translation>Üst</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="113"/>
        <source>Portrait (no rotation)</source>
        <translation>Portre (döndürme yok)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="114"/>
        <source>Landscape (90 degrees)</source>
        <translation>Manzara (90 derece)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="115"/>
        <source>Reverse landscape (270 degrees)</source>
        <translation>Ters manzara (270 derece)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="116"/>
        <source>Reverse portrait (180 degrees)</source>
        <translation>Ters portre (180 derece)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="117"/>
        <source>Auto Rotation</source>
        <translation>Otomatik Döndür</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="121"/>
        <source>Reverse</source>
        <translation>Ters</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="127"/>
        <source>Bind</source>
        <translation>Cilt</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="128"/>
        <source>Bind (none)</source>
        <translation>Cilt (yok)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="129"/>
        <source>Bind (bottom)</source>
        <translation>Cilt (alt)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="130"/>
        <source>Bind (left)</source>
        <translation>Cilt (sol)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="131"/>
        <source>Bind (right)</source>
        <translation>Cilt (sağ)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="132"/>
        <source>Bind (top)</source>
        <translation>Cilt (üst)</translation>
    </message>
</context>
<context>
    <name>DPrintersShowWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="81"/>
        <source>Settings</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="87"/>
        <source>Printers</source>
        <translation>Yazıcılar</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="91"/>
        <source>Add printer</source>
        <translation>Yazıcı ekle</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="94"/>
        <source>Delete printer</source>
        <translation>Yazıcı sil</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="115"/>
        <source>Shared</source>
        <translation>Paylaşılan</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="118"/>
        <source>Enabled</source>
        <translation>Etkin</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="121"/>
        <source>Accept jobs</source>
        <translation>İşleri kabul et</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="125"/>
        <source>Set as default</source>
        <translation>Varsayılan olarak ayarla</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="136"/>
        <source>No Printers</source>
        <translation>Yazıcı Yok</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="160"/>
        <source>Location:</source>
        <translation>Konum:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="164"/>
        <source>Model:</source>
        <translation>Model:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="167"/>
        <source>Status:</source>
        <translation>Durum:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="195"/>
        <source>Properties</source>
        <translation>Özellikler</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="205"/>
        <source>Print Queue</source>
        <translation>Yazdırma Kuyruğu</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="215"/>
        <source>Print Test Page</source>
        <translation>Test Sayfası Yazdır</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="237"/>
        <source>Supplies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="262"/>
        <source>No printer configured</source>
        <translation>Yapılandırılmış yazıcı yok</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="267"/>
        <source>Click + to add printers</source>
        <translation>Yazıcı eklemek için + işaretini tıkla</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="330"/>
        <source>As print jobs are in process, you cannot rename the printer now, please try later</source>
        <translation>Yazdırma işleri devam ederken, yazıcıyı adlandırılamaz, lütfen daha sonra deneyin</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="334"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="382"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="601"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="360"/>
        <source>Idle</source>
        <translation>Boş</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="362"/>
        <source>Printing</source>
        <translation>Yazdırılıyor</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="364"/>
        <source>Disabled</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="379"/>
        <source>CUPS server is not running, and can’t manage printers.</source>
        <translation>CUPS sunucusu çalışmıyor ve yazıcıları yönetemiyor.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="559"/>
        <source>Are you sure you want to delete the printer &quot;%1&quot; ?</source>
        <translation>&quot;%1&quot; yazıcısını silmek istediğinize emin misiniz?</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="565"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="597"/>
        <source>The name already exists</source>
        <translation>İsim zaten var</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="616"/>
        <source>You will not be able to reprint the completed jobs if continue. Are you sure you want to rename the printer?</source>
        <translation>Devam ederse tamamlanan işleri yeniden yazdıramazsınız. Yazıcıyı yeniden adlandırmak istediğinizden emin misiniz?</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="621"/>
        <source>Confirm</source>
        <translation>Onayla</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="679"/>
        <source>The driver is damaged, please install it again.</source>
        <translation>Sürücü hasarlı, lütfen tekrar kur.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="682"/>
        <source>Install Driver</source>
        <translation>Sürücü Kur</translation>
    </message>
</context>
<context>
    <name>DPropertySetDlg</name>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="175"/>
        <source>Print Properties</source>
        <translation>Yazdırma Özellikleri</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="176"/>
        <source>Driver</source>
        <translation>Sürücü</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="177"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="178"/>
        <source>Location</source>
        <translation>Konum</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="179"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="180"/>
        <source>Color Mode</source>
        <translation>Renk Kipi</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="181"/>
        <source>Orientation</source>
        <translation>Uyum</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="182"/>
        <source>Page Order</source>
        <translation>Sayfa Sırası</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="603"/>
        <source>Options conflict!</source>
        <translation>Seçenekler çakışması!</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="606"/>
        <source>Please resolve the conflict first, and then save the changes.</source>
        <translation>Lütfen önce çakışmayı çözümleyin ve sonra değişiklikleri kaydedin.</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="625"/>
        <source>Conflict:</source>
        <translation>Çakışma:</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="636"/>
        <location filename="../ui/dpropertysetdlg.cpp" line="1123"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="1122"/>
        <source>Invalid URI</source>
        <translation>Geçersiz URI</translation>
    </message>
</context>
<context>
    <name>DriverManager</name>
    <message>
        <location filename="../vendor/zdrivermanager.cpp" line="791"/>
        <source>EveryWhere driver</source>
        <translation>Her yerde sürücü</translation>
    </message>
</context>
<context>
    <name>InstallDriver</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="374"/>
        <source>Failed to find the driver solution: %1, error: %2</source>
        <translation>Sürücü çözümü bulunamadı: %1, hata: %2</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="385"/>
        <source>The solution is invalid</source>
        <translation>Çözüm geçersiz</translation>
    </message>
</context>
<context>
    <name>InstallDriverWindow</name>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="123"/>
        <source>Select a driver from</source>
        <translation>Bir sürücü seç</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="127"/>
        <source>Local driver</source>
        <translation>Yerel sürücü</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="128"/>
        <source>Local PPD file</source>
        <translation>Yerel PPD dosyası</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="129"/>
        <source>Search for a driver</source>
        <translation>Sürücü ara</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="143"/>
        <location filename="../ui/installdriverwindow.cpp" line="421"/>
        <source>Choose a local driver</source>
        <translation>Yerel bir sürücü seç</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="146"/>
        <source>Vendor</source>
        <translation>Marka</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="158"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="169"/>
        <location filename="../ui/installdriverwindow.cpp" line="238"/>
        <source>Driver</source>
        <translation>Sürücü</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="223"/>
        <source>Vendor and Model</source>
        <translation>Marka ve Model</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="225"/>
        <source>Enter a complete vendor and model (Only letters, numbers and whitespaces)</source>
        <translation>Eksiksiz bir marka ve model girin (Yalnızca harfler, sayılar ve boşluklar)</translation>
    </message>
    <message>
        <source>Enter a complete vendor and model</source>
        <translation type="vanished">Tam bir marka ve model girin</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="227"/>
        <source>Search</source>
        <translation>Ara</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="268"/>
        <source>Install Driver</source>
        <translation>Sürücü Kur</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="403"/>
        <location filename="../ui/installdriverwindow.cpp" line="515"/>
        <source>Reselect</source>
        <translation>Yeniden seç</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="429"/>
        <location filename="../ui/installdriverwindow.cpp" line="512"/>
        <source>Select a PPD file</source>
        <translation>Bir PPD dosyası seç</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="438"/>
        <source>Search for printer driver</source>
        <translation>Yazıcı sürücüsünü ara</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="494"/>
        <source>(recommended)</source>
        <translation>(önerilen)</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="656"/>
        <source> (recommended)</source>
        <translation>(önerilen)</translation>
    </message>
</context>
<context>
    <name>InstallInterface</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="252"/>
        <source>is invalid</source>
        <translation>geçersizdir</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="281"/>
        <source>Failed to install the driver by calling dbus interface</source>
        <translation>Dbus arayüzünü çağırarak sürücü kurulamadı</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="326"/>
        <source>Failed to install %1</source>
        <translation>%1 kurulamadı</translation>
    </message>
</context>
<context>
    <name>InstallPrinterWindow</name>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="85"/>
        <location filename="../ui/installprinterwindow.cpp" line="148"/>
        <source>Installing driver...</source>
        <translation>Sürücü kuruluyor...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="95"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="98"/>
        <location filename="../ui/installprinterwindow.cpp" line="171"/>
        <location filename="../ui/installprinterwindow.cpp" line="188"/>
        <source>View Printer</source>
        <translation>Yazıcıyı Göster</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="100"/>
        <location filename="../ui/installprinterwindow.cpp" line="173"/>
        <location filename="../ui/installprinterwindow.cpp" line="190"/>
        <source>Print Test Page</source>
        <translation>Test Sayfası Yazdır</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="160"/>
        <source>Successfully installed </source>
        <translation>Başarıyla kuruldu</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="166"/>
        <location filename="../ui/installprinterwindow.cpp" line="184"/>
        <source>You have successfully added the printer. Print a test page to check if it works properly.</source>
        <translation>Yazıcıyı başarıyla eklediniz. Düzgün çalışıp çalışmadığını kontrol etmek için bir test sayfası yazdırın.</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="178"/>
        <source>Printing test page...</source>
        <translation>Test sayfası yazdırılıyor...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="195"/>
        <source>Did you print the test page successfully?</source>
        <translation>Test sayfasını başarıyla yazdırdınız mı?</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="201"/>
        <source>If succeeded, click Yes; if failed, click No</source>
        <translation>Başarılı olursa, Evet; başarısız olursa Hayır&apos;ı tıkla</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="204"/>
        <source>No</source>
        <translation>Hayır</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="206"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="211"/>
        <location filename="../ui/installprinterwindow.cpp" line="231"/>
        <source>Print failed</source>
        <translation>Yazdırma başarısız</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="217"/>
        <source>Click Reinstall to install the printer driver again, or click Troubleshoot to start troubleshooting.</source>
        <translation>Yazıcı sürücüsünü tekrar kurmak için Yeniden Kur&apos;a tıkla veya sorun gidermeye başlamak için Sorun Gider&apos;i tıkla.</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="220"/>
        <location filename="../ui/installprinterwindow.cpp" line="241"/>
        <source>Reinstall</source>
        <translation>Yeniden kur</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="222"/>
        <location filename="../ui/installprinterwindow.cpp" line="243"/>
        <source>Troubleshoot</source>
        <translation>Sorun gider</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="229"/>
        <source>Installation failed</source>
        <translation>Kurulum başarısız</translation>
    </message>
</context>
<context>
    <name>JobListView</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="214"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="371"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="215"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="372"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="216"/>
        <source>Pause</source>
        <translation>Duraklat</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="217"/>
        <source>Resume</source>
        <translation>Devam</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="218"/>
        <source>Print first</source>
        <translation>İlk önce yazdır</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="219"/>
        <source>Reprint</source>
        <translation>Yeniden yazdır</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="221"/>
        <source>No print jobs</source>
        <translation>Yazdırma işi yok</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="365"/>
        <source>Are you sure you want to delete this job?</source>
        <translation>Bu işi silmek istediğinden emin misin?</translation>
    </message>
</context>
<context>
    <name>JobManager</name>
    <message>
        <location filename="../vendor/zjobmanager.cpp" line="279"/>
        <source> not found</source>
        <translation>bulunamadı</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1007"/>
        <source>Refresh</source>
        <translation>Yenile</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1011"/>
        <source>All</source>
        <translation>Tümü</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1012"/>
        <source>Print Queue</source>
        <translation>Yazdırma Kuyruğu</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1013"/>
        <source>Completed</source>
        <translation>Tamamlandı</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1070"/>
        <source> failed</source>
        <translation>başarısız</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1091"/>
        <source>%1 jobs</source>
        <translation>%1 iş</translation>
    </message>
</context>
<context>
    <name>JobsDataModel</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="631"/>
        <source>Job</source>
        <translation>İş</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="633"/>
        <source>User</source>
        <translation>Kullanıcı</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="635"/>
        <source>Document</source>
        <translation>Belge</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="637"/>
        <source>Printer</source>
        <translation>Yazıcı</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="639"/>
        <source>Size</source>
        <translation>Boyut</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="641"/>
        <source>Time submitted</source>
        <translation>Gönderilen zaman</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="643"/>
        <source>Status</source>
        <translation>Durum</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="645"/>
        <source>Action</source>
        <translation>Eylem</translation>
    </message>
</context>
<context>
    <name>PermissionsWindow</name>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="48"/>
        <source>Connect to %1 to find a printer</source>
        <translation>Bir yazıcı bulmak için %1&apos;e bağlan</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="74"/>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="76"/>
        <source>Group</source>
        <translation>Küme</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="78"/>
        <source>Password</source>
        <translation>Parola</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="92"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="93"/>
        <source>Connect</source>
        <translation>Bağlan</translation>
    </message>
</context>
<context>
    <name>PrinterApplication</name>
    <message>
        <location filename="../ui/printerapplication.cpp" line="111"/>
        <source>Print Manager</source>
        <translation>Yazıcı Yöneticisi</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="112"/>
        <source>Print Manager is a printer management tool, which supports adding and removing printers, managing print jobs and so on.</source>
        <translation>Yazıcı Yöneticisi, yazıcı ekleme ve kaldırmayı, yazdırma işlerini yönetmeyi vb işleri destekleyen bir yazıcı yönetim aracıdır.</translation>
    </message>
</context>
<context>
    <name>PrinterSearchWindow</name>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="83"/>
        <location filename="../ui/printersearchwindow.cpp" line="86"/>
        <source>Discover printer</source>
        <translation>Yazıcıyı keşfet</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="89"/>
        <location filename="../ui/printersearchwindow.cpp" line="92"/>
        <source>Find printer</source>
        <translation>Yazıcı bul</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="94"/>
        <location filename="../ui/printersearchwindow.cpp" line="97"/>
        <source>Enter URI</source>
        <translation>URI gir</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="118"/>
        <source>Select a printer</source>
        <translation>Bir yazıcı seç</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="124"/>
        <source>Refresh</source>
        <translation>Yenile</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="162"/>
        <source>Install Driver</source>
        <translation>Sürücü Kur</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="190"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="192"/>
        <source>Enter an address</source>
        <translation>Bir adres gir</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="193"/>
        <source>Find</source>
        <translation>Bul</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="266"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="270"/>
        <source>Enter device URI</source>
        <translation>Cihaz URI&apos;sini gir</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="281"/>
        <source>Examples:</source>
        <translation>Örnekler:</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="556"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="644"/>
        <source> (recommended)</source>
        <translation>(önerilen)</translation>
    </message>
</context>
<context>
    <name>PrinterTestJob</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="369"/>
        <source>Check test page printing</source>
        <translation>Test sayfası yazdırmasını kontrol et</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="416"/>
        <source>Printing test page...</source>
        <translation>Test sayfası yazdırılıyor...</translation>
    </message>
</context>
<context>
    <name>PrinterTestPageDialog</name>
    <message>
        <location filename="../ui/printertestpagedialog.cpp" line="63"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="103"/>
        <source>1 min ago</source>
        <translation>1 dakika önce</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="106"/>
        <source>%1 mins ago</source>
        <translation>%1 dakika önce</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="110"/>
        <source>1 hr ago</source>
        <translation>1 saat önce</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="112"/>
        <source>%1 hrs ago</source>
        <translation>%1 saat önce</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="117"/>
        <source>Yesterday</source>
        <translation>Dün</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="119"/>
        <source>%1 days ago</source>
        <translation>%1 gün önce</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="481"/>
        <source>(use %1 protocol)</source>
        <translation>(%1 protokolünü kullan)</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="26"/>
        <source>Select a driver</source>
        <translation>Bir sürücü seç</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="32"/>
        <source>Driver</source>
        <translation>Sürücü</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="33"/>
        <source>Next</source>
        <translation>İleri</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="34"/>
        <source>Install Driver</source>
        <translation>Sürücü Kur</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="36"/>
        <source>Drag a PPD file here 
 or</source>
        <translation>PPD dosyasını buraya sürükle
yada</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="37"/>
        <source>Select a PPD file</source>
        <translation>Bir PPD dosyası seç</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="39"/>
        <source>Troubleshoot</source>
        <translation>Sorun gider</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="40"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../vendor/common.cpp" line="149"/>
        <source> not found, please ask the administrator for help</source>
        <translation>bulunamadı, lütfen yöneticiden yardım isteyin</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="121"/>
        <source>Direct-attached Device</source>
        <translation>Doğrudan bağlı Cihaz</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="122"/>
        <source>File</source>
        <translation>Dosya</translation>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="255"/>
        <source>Black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="256"/>
        <source>Blue</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="257"/>
        <source>Brown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="258"/>
        <source>Cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="259"/>
        <source>Dark-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="260"/>
        <source>Dark gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="261"/>
        <source>Dark-yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="262"/>
        <source>Dark yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="263"/>
        <source>Gold</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="264"/>
        <source>Gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="265"/>
        <source>Green</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="266"/>
        <source>Light-black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="267"/>
        <source>Light black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="268"/>
        <source>Light-cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="269"/>
        <source>Light cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="270"/>
        <source>Light-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="271"/>
        <source>Light gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="272"/>
        <source>Light-magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="273"/>
        <source>Light magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="274"/>
        <source>Magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="275"/>
        <source>Orange</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="276"/>
        <source>Red</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="277"/>
        <source>Silver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="278"/>
        <source>White</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="279"/>
        <source>Yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="280"/>
        <source>Waste</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RenamePrinterWindow</name>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="84"/>
        <source>Rename Printer</source>
        <translation>Yazıcıyı Adlandır</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="91"/>
        <source>Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="93"/>
        <source>Location</source>
        <translation>Konum</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="95"/>
        <source>Description</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="106"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="107"/>
        <source>Confirm</source>
        <translation>Onayla</translation>
    </message>
</context>
<context>
    <name>ServerSettingsWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="85"/>
        <source>Basic Server Settings</source>
        <translation>Basit Sunucu Ayarları</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="93"/>
        <source>Publish shared printers connected to this system</source>
        <translation>Bu sisteme bağlı paylaşılan yazıcıları yayınla</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="94"/>
        <source>Allow printing from the Internet</source>
        <translation>İnternetten yazdırmaya izin ver</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="96"/>
        <source>Allow remote administration</source>
        <translation>Uzaktan yönetime izin ver</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="98"/>
        <source>Save debugging information for troubleshooting</source>
        <translation>Sorun giderme için hata ayıklama bilgilerini kaydet</translation>
    </message>
</context>
<context>
    <name>TroubleShootDialog</name>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="109"/>
        <source>Troubleshoot: </source>
        <translation>Sorun gider:</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="134"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="165"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
</context>
</TS>