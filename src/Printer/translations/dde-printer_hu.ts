<?xml version="1.0" ?><!DOCTYPE TS><TS language="hu" version="2.1">
<context>
    <name>AddPrinterTask</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="547"/>
        <source>URI and driver do not match.</source>
        <translation>Az url és a driver nem passzol egymáshoz.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="549"/>
        <source>Install hplip first and restart the app to install the driver again.</source>
        <translation>Ahhoz hogy sikeres legyen a driver telepítése, először telepíteni kell hplip-et, majd újra kell indítani az alkalmazást.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="557"/>
        <source>Please select an hplip driver and try again.</source>
        <translation>Válasszon egy hplip drivert és próbálja újra.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="570"/>
        <source>URI can&apos;t be empty</source>
        <translation>az URL nem lehet üres.</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="577"/>
        <source> not found</source>
        <translation>nem található</translation>
    </message>
</context>
<context>
    <name>CheckAttributes</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="306"/>
        <source>Check printer settings</source>
        <translation>Ellenőrizze a nyomtató beállításait</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="315"/>
        <source>Checking printer settings...</source>
        <translation>Nyomtató beállításainak ellenőrzése...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="318"/>
        <source>Success</source>
        <translation>Sikerült</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="326"/>
        <source>Failed to get printer attributes, error: </source>
        <translation>Sikertelen elérés a nyomtató attribútumaihoz. Hiba:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="334"/>
        <source>%1 is disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="343"/>
        <source>is not accepting jobs</source>
        <translation>nem fogad munkákat</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="348"/>
        <source>Printer settings are ok</source>
        <translation>A nyomtató beállításai jók</translation>
    </message>
</context>
<context>
    <name>CheckConnected</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="211"/>
        <source>Check printer connection</source>
        <translation>Ellenőrizze a nyomtatás kapcsolatát</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="223"/>
        <source>Checking printer connection...</source>
        <translation>Nyomtató kapcsolatának ellenőrzése...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="226"/>
        <source>Success</source>
        <translation>Sikerült</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="263"/>
        <source>Cannot connect to the printer, error: %1</source>
        <translation>Nem sikerült kapcsolódni a nyomtatóhoz, hiba: %1</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="271"/>
        <source> is not connected, URI: </source>
        <translation>nem kapcsolódott: URL:</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="280"/>
        <source>%1 does not exist</source>
        <translation>%1 nem létezik</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="289"/>
        <source>Cannot connect to the printer</source>
        <translation>Nem tud kapcsolódni a nyomtatóhoz</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="295"/>
        <source>The connection is valid</source>
        <translation>A kapcsolat érvényesítve</translation>
    </message>
</context>
<context>
    <name>CheckCupsServer</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="103"/>
        <source>Check CUPS server</source>
        <translation>Ellenőrizze a CUPS szervert</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="111"/>
        <source>Checking CUPS server...</source>
        <translation>CUPS szerver ellenőrzése...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="117"/>
        <source>CUPS server is invalid</source>
        <translation>A CUPS szerver érvénytelen</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="122"/>
        <source>CUPS server is valid</source>
        <translation>A CUPS szerver érvényes</translation>
    </message>
</context>
<context>
    <name>CheckDriver</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="140"/>
        <source>Checking driver...</source>
        <translation>Driver ellenőrzése...</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="143"/>
        <source>Success</source>
        <translation>Sikerült</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="151"/>
        <source>PPD file %1 not found</source>
        <translation>A %1 PPD fájl nem található</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="161"/>
        <source>The driver is damaged</source>
        <translation>A driver sérült</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="177"/>
        <source>Driver filter %1 not found</source>
        <translation>A driver szűrő %1 nem található</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="189"/>
        <source>%1 is not installed, cannot print now</source>
        <translation>%1 nincs telepítve, nem lehet most nyomtatni</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="195"/>
        <source>Driver is valid</source>
        <translation>A driver érvényes</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="201"/>
        <source>Check driver</source>
        <translation>Ellenőrizze a drivert</translation>
    </message>
</context>
<context>
    <name>CupsMonitor</name>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="74"/>
        <source>Queuing</source>
        <translation>Sorba állítás</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="75"/>
        <source>Paused</source>
        <translation>Felfüggesztve</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="76"/>
        <source>Printing</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="77"/>
        <source>Stopped</source>
        <translation>Megállítva</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="78"/>
        <source>Canceled</source>
        <translation>Törölve</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="79"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="80"/>
        <source>Completed</source>
        <translation>Teljesítve</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="293"/>
        <source>%1 timed out, reason: %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="307"/>
        <source>%1 printed successfully, please take away the paper in time!</source>
        <translation>%1 sikeresen kinyomtatva, vegye ki időben a papírt!</translation>
    </message>
    <message>
        <location filename="../vendor/zcupsmonitor.cpp" line="309"/>
        <source>%1 %2, reason: %3</source>
        <translation>%1 %2, oka: %3</translation>
    </message>
</context>
<context>
    <name>DDestination</name>
    <message>
        <location filename="../util/ddestination.cpp" line="148"/>
        <location filename="../util/ddestination.cpp" line="202"/>
        <source>Unknown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="209"/>
        <source>Idle</source>
        <translation>várakozik</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="212"/>
        <source>Printing</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../util/ddestination.cpp" line="215"/>
        <source>Disabled</source>
        <translation>Letiltva</translation>
    </message>
</context>
<context>
    <name>DPrinterSupplyShowDlg</name>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="75"/>
        <source>Ink/Toner Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="81"/>
        <source>Unknown amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="88"/>
        <source>Unable to get the remaining amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="113"/>
        <source>The amounts are estimated, last updated at %1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The amounts are estimated, last updated at%1:%2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="122"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="221"/>
        <source>Unknown</source>
        <translation>Ismeretlen</translation>
    </message>
</context>
<context>
    <name>DPrinterTanslator</name>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="33"/>
        <source>Color</source>
        <translation>Szín</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="34"/>
        <location filename="../util/dprintertanslator.cpp" line="35"/>
        <source>Grayscale</source>
        <translation>Szürkeárnyalatos</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="38"/>
        <location filename="../util/dprintertanslator.cpp" line="69"/>
        <location filename="../util/dprintertanslator.cpp" line="86"/>
        <location filename="../util/dprintertanslator.cpp" line="107"/>
        <location filename="../util/dprintertanslator.cpp" line="126"/>
        <location filename="../util/dprintertanslator.cpp" line="135"/>
        <source>None</source>
        <translation>Semmi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="39"/>
        <location filename="../util/dprintertanslator.cpp" line="49"/>
        <source>Draft</source>
        <translation>Vázlat</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="40"/>
        <location filename="../util/dprintertanslator.cpp" line="120"/>
        <source>Normal</source>
        <translation>Normál</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="43"/>
        <location filename="../util/dprintertanslator.cpp" line="44"/>
        <location filename="../util/dprintertanslator.cpp" line="47"/>
        <source>Print Quality</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="55"/>
        <location filename="../util/dprintertanslator.cpp" line="56"/>
        <source>Auto-Select</source>
        <translation>Automata kiválasztás</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="57"/>
        <location filename="../util/dprintertanslator.cpp" line="58"/>
        <source>Manual Feeder</source>
        <translation>Külső tálcaadagoló</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="59"/>
        <location filename="../util/dprintertanslator.cpp" line="73"/>
        <source>Auto</source>
        <translation>Automatikus</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="48"/>
        <location filename="../util/dprintertanslator.cpp" line="60"/>
        <source>Manual</source>
        <translation>Kézi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="52"/>
        <location filename="../util/dprintertanslator.cpp" line="53"/>
        <location filename="../util/dprintertanslator.cpp" line="54"/>
        <source>Paper Source</source>
        <translation>Papír forrása</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="61"/>
        <source>Drawer 1</source>
        <translation>1-es fiók</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="62"/>
        <source>Drawer 2</source>
        <translation>2-es fiók</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="63"/>
        <source>Drawer 3</source>
        <translation>3-as fiók</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="64"/>
        <source>Drawer 4</source>
        <translation>4-es fiók</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="65"/>
        <source>Drawer 5</source>
        <translation>5-ös fiók</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="66"/>
        <source>Envelope Feeder</source>
        <translation>Boríték adagoló</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="67"/>
        <source>Tray1</source>
        <translation>1-es tálca</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="68"/>
        <source>Unknown</source>
        <translation>Ismeretlen</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="72"/>
        <source>MediaType</source>
        <translation>Adathordozó tipusa</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="74"/>
        <source>Plain Paper</source>
        <translation>Egyszerű papír</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="75"/>
        <source>Recycled Paper</source>
        <translation>Újrahasznosított papír</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="76"/>
        <source>Color Paper</source>
        <translation>Színes papír</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="77"/>
        <source>Bond Paper</source>
        <translation>Finom papír</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="78"/>
        <source>Heavy Paper 1</source>
        <translation>Kemény papír 1</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="79"/>
        <source>Heavy Paper 2</source>
        <translation>Kemény papír 2</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="80"/>
        <source>Heavy Paper 3</source>
        <translation>Kemény papír 3</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="81"/>
        <location filename="../util/dprintertanslator.cpp" line="82"/>
        <source>OHP</source>
        <translation>OHP</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="83"/>
        <source>Labels</source>
        <translation>Cimkék</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="84"/>
        <source>Envelope</source>
        <translation>Boríték</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="85"/>
        <source>Photo Paper</source>
        <translation>Fotópapír</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="89"/>
        <location filename="../util/dprintertanslator.cpp" line="90"/>
        <location filename="../util/dprintertanslator.cpp" line="91"/>
        <source>PageSize</source>
        <translation>Oldal méret</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="92"/>
        <source>Custom</source>
        <translation>Egyedi</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="95"/>
        <location filename="../util/dprintertanslator.cpp" line="96"/>
        <location filename="../util/dprintertanslator.cpp" line="97"/>
        <source>Duplex</source>
        <translation>Duplex</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="98"/>
        <source>DuplexTumble</source>
        <translation>Dupla-átfordítás</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="99"/>
        <source>DuplexNoTumble</source>
        <translation>Dupla-átfordítás nélkül</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="100"/>
        <source>ON (Long-edged Binding)</source>
        <translation>Be (Hosszú oldali kötésben)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="101"/>
        <source>ON (Short-edged Binding)</source>
        <translation>Be (Rövid oldali kötésben)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="102"/>
        <location filename="../util/dprintertanslator.cpp" line="103"/>
        <source>OFF</source>
        <translation>Ki</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="106"/>
        <location filename="../util/dprintertanslator.cpp" line="110"/>
        <source>Binding Edge</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="124"/>
        <location filename="../util/dprintertanslator.cpp" line="125"/>
        <source>Staple Location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="136"/>
        <location filename="../util/dprintertanslator.cpp" line="137"/>
        <source>Resolution</source>
        <translation>Felbontás</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="108"/>
        <source>Left</source>
        <translation>Bal</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="109"/>
        <source>Top</source>
        <translation>Fent</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="113"/>
        <source>Portrait (no rotation)</source>
        <translation>Álló (forgatás nélkül)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="114"/>
        <source>Landscape (90 degrees)</source>
        <translation>Fekvő (90 fokban)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="115"/>
        <source>Reverse landscape (270 degrees)</source>
        <translation>Fordított fekvő (270 fokban)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="116"/>
        <source>Reverse portrait (180 degrees)</source>
        <translation>Fordított álló (180 fokban)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="117"/>
        <source>Auto Rotation</source>
        <translation>Automata elforgatás</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="121"/>
        <source>Reverse</source>
        <translation>Visszafelé</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="127"/>
        <source>Bind</source>
        <translation>Kötés</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="128"/>
        <source>Bind (none)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="129"/>
        <source>Bind (bottom)</source>
        <translation>Kötésben (alul)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="130"/>
        <source>Bind (left)</source>
        <translation>Kötésben (bal oldalon)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="131"/>
        <source>Bind (right)</source>
        <translation>Kötésben (jobb oldalon)</translation>
    </message>
    <message>
        <location filename="../util/dprintertanslator.cpp" line="132"/>
        <source>Bind (top)</source>
        <translation>Kötésben (fent)</translation>
    </message>
</context>
<context>
    <name>DPrintersShowWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="81"/>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="87"/>
        <source>Printers</source>
        <translation>Nyomtatók</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="91"/>
        <source>Add printer</source>
        <translation>Nyomtató hozzáadása</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="94"/>
        <source>Delete printer</source>
        <translation>Nyomtató eltávolítása</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="115"/>
        <source>Shared</source>
        <translation>Megosztva</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="118"/>
        <source>Enabled</source>
        <translation>Engedélyezve</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="121"/>
        <source>Accept jobs</source>
        <translation>Munkák fogadása</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="125"/>
        <source>Set as default</source>
        <translation>Beállítás alapértelmezettként</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="136"/>
        <source>No Printers</source>
        <translation>Nincsenek nyomtatók</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="160"/>
        <source>Location:</source>
        <translation>Hely:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="164"/>
        <source>Model:</source>
        <translation>Tipus:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="167"/>
        <source>Status:</source>
        <translation>Státusz:</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="195"/>
        <source>Properties</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="205"/>
        <source>Print Queue</source>
        <translation>Nyomtatási sor</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="215"/>
        <source>Print Test Page</source>
        <translation>Tesztoldal nyomtatás</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="237"/>
        <source>Supplies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="262"/>
        <source>No printer configured</source>
        <translation>A nyomtató nincs beállítva</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="267"/>
        <source>Click + to add printers</source>
        <translation>Kattints a + jelre a nyomtató hozzáadásához</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="330"/>
        <source>As print jobs are in process, you cannot rename the printer now, please try later</source>
        <translation>Amíg a nyomtatás folymatban van, nem lehet átnevezni a nyomtatót. Próbálja később.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="334"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="382"/>
        <location filename="../ui/dprintersshowwindow.cpp" line="601"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="360"/>
        <source>Idle</source>
        <translation>Várakozik</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="362"/>
        <source>Printing</source>
        <translation>Nyomtatás</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="364"/>
        <source>Disabled</source>
        <translation>Letiltva</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="379"/>
        <source>CUPS server is not running, and can’t manage printers.</source>
        <translation>A CUPS szerver nem fut, nem tudja kezelni a nyomtatókat.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="559"/>
        <source>Are you sure you want to delete the printer &quot;%1&quot; ?</source>
        <translation>Biztosan törölni szeretné ezt a nyomtatót: &quot;%1&quot; ?</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="565"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="597"/>
        <source>The name already exists</source>
        <translation>Ez a név már létezik</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="616"/>
        <source>You will not be able to reprint the completed jobs if continue. Are you sure you want to rename the printer?</source>
        <translation>Nem fogja tudni újranyomtatni a már kinyomtatott munkákat, ha folytatja. Biztosan át akarja nevezni a nyomtatót?</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="621"/>
        <source>Confirm</source>
        <translation>Megerősítés</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="679"/>
        <source>The driver is damaged, please install it again.</source>
        <translation>A driver megsérült, kérem telepítse újra.</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.cpp" line="682"/>
        <source>Install Driver</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>DPropertySetDlg</name>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="175"/>
        <source>Print Properties</source>
        <translation>Nyomtatás beállításai</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="176"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="177"/>
        <source>URI</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="178"/>
        <source>Location</source>
        <translation>Hely</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="179"/>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="180"/>
        <source>Color Mode</source>
        <translation>Színmód</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="181"/>
        <source>Orientation</source>
        <translation>Orientáció</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="182"/>
        <source>Page Order</source>
        <translation>Oldal sorrendje</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="603"/>
        <source>Options conflict!</source>
        <translation>Ütköző beállítások!</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="606"/>
        <source>Please resolve the conflict first, and then save the changes.</source>
        <translation>Először szüntesse meg a problémákat okozó beállításokat, ezután mentse el a változtatásokat.</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="625"/>
        <source>Conflict:</source>
        <translation>Ütközés:</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="636"/>
        <location filename="../ui/dpropertysetdlg.cpp" line="1123"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/dpropertysetdlg.cpp" line="1122"/>
        <source>Invalid URI</source>
        <translation>Érvénytelen URL</translation>
    </message>
</context>
<context>
    <name>DriverManager</name>
    <message>
        <location filename="../vendor/zdrivermanager.cpp" line="791"/>
        <source>EveryWhere driver</source>
        <translation>EveryWhere driver</translation>
    </message>
</context>
<context>
    <name>InstallDriver</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="374"/>
        <source>Failed to find the driver solution: %1, error: %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="385"/>
        <source>The solution is invalid</source>
        <translation>Érvénytelen megoldás</translation>
    </message>
</context>
<context>
    <name>InstallDriverWindow</name>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="123"/>
        <source>Select a driver from</source>
        <translation>Válassza ki a drivert</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="127"/>
        <source>Local driver</source>
        <translation>Helyi driver</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="128"/>
        <source>Local PPD file</source>
        <translation>Helyi PPD file</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="129"/>
        <source>Search for a driver</source>
        <translation>Driver keresése</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="143"/>
        <location filename="../ui/installdriverwindow.cpp" line="421"/>
        <source>Choose a local driver</source>
        <translation>Válasszon egy helyi drivert</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="146"/>
        <source>Vendor</source>
        <translation>Gyártó</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="158"/>
        <source>Model</source>
        <translation>Modell</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="169"/>
        <location filename="../ui/installdriverwindow.cpp" line="238"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="223"/>
        <source>Vendor and Model</source>
        <translation>Gyártó és modell</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="225"/>
        <source>Enter a complete vendor and model (Only letters, numbers and whitespaces)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Enter a complete vendor and model</source>
        <translation type="vanished">Írja be a gyártót és a tipus számot</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="227"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="268"/>
        <source>Install Driver</source>
        <translation>Driver telepítés</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="403"/>
        <location filename="../ui/installdriverwindow.cpp" line="515"/>
        <source>Reselect</source>
        <translation>Újra kiválasztás</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="429"/>
        <location filename="../ui/installdriverwindow.cpp" line="512"/>
        <source>Select a PPD file</source>
        <translation>Válasszon egy PPD fájlt</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="438"/>
        <source>Search for printer driver</source>
        <translation>Nyomtató driver keresése</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="494"/>
        <source>(recommended)</source>
        <translation>(ajánlott)</translation>
    </message>
    <message>
        <location filename="../ui/installdriverwindow.cpp" line="656"/>
        <source> (recommended)</source>
        <translation>(ajánlott)</translation>
    </message>
</context>
<context>
    <name>InstallInterface</name>
    <message>
        <location filename="../vendor/addprinter.cpp" line="252"/>
        <source>is invalid</source>
        <translation>érvénytelen</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="281"/>
        <source>Failed to install the driver by calling dbus interface</source>
        <translation>Nem sikerült telepíteni a drivert a dbus onterfész lehívás miatt</translation>
    </message>
    <message>
        <location filename="../vendor/addprinter.cpp" line="326"/>
        <source>Failed to install %1</source>
        <translation>Nem sikerült telepíteni %1</translation>
    </message>
</context>
<context>
    <name>InstallPrinterWindow</name>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="85"/>
        <location filename="../ui/installprinterwindow.cpp" line="148"/>
        <source>Installing driver...</source>
        <translation>Driver telepítése...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="95"/>
        <source>Cancel</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="98"/>
        <location filename="../ui/installprinterwindow.cpp" line="171"/>
        <location filename="../ui/installprinterwindow.cpp" line="188"/>
        <source>View Printer</source>
        <translation>Nyomtató nézete</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="100"/>
        <location filename="../ui/installprinterwindow.cpp" line="173"/>
        <location filename="../ui/installprinterwindow.cpp" line="190"/>
        <source>Print Test Page</source>
        <translation>Tesztoldal nyomtatás</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="160"/>
        <source>Successfully installed </source>
        <translation>Sikeresen telepítve</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="166"/>
        <location filename="../ui/installprinterwindow.cpp" line="184"/>
        <source>You have successfully added the printer. Print a test page to check if it works properly.</source>
        <translation>Sikerült feltelepíteni a nyomtatót. Nyomtasson egy tesztoldalt hogy leellenőrizze a nyomtató helyes működését.</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="178"/>
        <source>Printing test page...</source>
        <translation>Tesztoldal nyomtatása...</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="195"/>
        <source>Did you print the test page successfully?</source>
        <translation>Sikerült kinyomtatni a teszoldalt?</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="201"/>
        <source>If succeeded, click Yes; if failed, click No</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="204"/>
        <source>No</source>
        <translation>Nem</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="206"/>
        <source>Yes</source>
        <translation>Igen</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="211"/>
        <location filename="../ui/installprinterwindow.cpp" line="231"/>
        <source>Print failed</source>
        <translation>%1 napja</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="217"/>
        <source>Click Reinstall to install the printer driver again, or click Troubleshoot to start troubleshooting.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="220"/>
        <location filename="../ui/installprinterwindow.cpp" line="241"/>
        <source>Reinstall</source>
        <translation>Újratelepítés</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="222"/>
        <location filename="../ui/installprinterwindow.cpp" line="243"/>
        <source>Troubleshoot</source>
        <translation>Hibamegoldás</translation>
    </message>
    <message>
        <location filename="../ui/installprinterwindow.cpp" line="229"/>
        <source>Installation failed</source>
        <translation>Sikertelen telepítés</translation>
    </message>
</context>
<context>
    <name>JobListView</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="214"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="371"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="215"/>
        <location filename="../ui/jobmanagerwindow.cpp" line="372"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="216"/>
        <source>Pause</source>
        <translation>Megállítás</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="217"/>
        <source>Resume</source>
        <translation>Folytatás</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="218"/>
        <source>Print first</source>
        <translation>Először nyomtasson</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="219"/>
        <source>Reprint</source>
        <translation>Újranyomtatás</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="221"/>
        <source>No print jobs</source>
        <translation>Nincs nyomtatnivaló</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="365"/>
        <source>Are you sure you want to delete this job?</source>
        <translation>Biztosan törölni akarja ezt a munkát?</translation>
    </message>
</context>
<context>
    <name>JobManager</name>
    <message>
        <location filename="../vendor/zjobmanager.cpp" line="279"/>
        <source> not found</source>
        <translation>nem található</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1007"/>
        <source>Refresh</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1011"/>
        <source>All</source>
        <translation>Mind</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1012"/>
        <source>Print Queue</source>
        <translation>Nyomtatási sor</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1013"/>
        <source>Completed</source>
        <translation>Kész</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1070"/>
        <source> failed</source>
        <translation>sikertelen</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="1091"/>
        <source>%1 jobs</source>
        <translation>%1 munka</translation>
    </message>
</context>
<context>
    <name>JobsDataModel</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="631"/>
        <source>Job</source>
        <translation>Munka</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="633"/>
        <source>User</source>
        <translation>Felhasználó</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="635"/>
        <source>Document</source>
        <translation>Dokumentum</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="637"/>
        <source>Printer</source>
        <translation>Nyomtató</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="639"/>
        <source>Size</source>
        <translation>Méret</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="641"/>
        <source>Time submitted</source>
        <translation>Idő beküldése</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="643"/>
        <source>Status</source>
        <translation>Státusz</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="645"/>
        <source>Action</source>
        <translation>Esemény</translation>
    </message>
</context>
<context>
    <name>PermissionsWindow</name>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="48"/>
        <source>Connect to %1 to find a printer</source>
        <translation>Kapcsolódjon ehhez: %1 hogy megtalálja a nyomtatót</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="74"/>
        <source>Username</source>
        <translation>Felhasználói név</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="76"/>
        <source>Group</source>
        <translation>Csoport</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="78"/>
        <source>Password</source>
        <translation>Jelszó</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="92"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../ui/permissionswindow.cpp" line="93"/>
        <source>Connect</source>
        <translation>Kapcsolódás</translation>
    </message>
</context>
<context>
    <name>PrinterApplication</name>
    <message>
        <location filename="../ui/printerapplication.cpp" line="111"/>
        <source>Print Manager</source>
        <translation>Nyomtatás kezelő</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="112"/>
        <source>Print Manager is a printer management tool, which supports adding and removing printers, managing print jobs and so on.</source>
        <translation>Nyomtatás kezelő egy olyan eszköz, amivel hozzá lehet adni sé el lehet távolítani nyomtatókat, kezelni lehet a nyomtatási munkákat, és így tovább.</translation>
    </message>
</context>
<context>
    <name>PrinterSearchWindow</name>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="83"/>
        <location filename="../ui/printersearchwindow.cpp" line="86"/>
        <source>Discover printer</source>
        <translation>Nyomtató felfedezése</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="89"/>
        <location filename="../ui/printersearchwindow.cpp" line="92"/>
        <source>Find printer</source>
        <translation>Nyomtató megtalálása</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="94"/>
        <location filename="../ui/printersearchwindow.cpp" line="97"/>
        <source>Enter URI</source>
        <translation>Írja be az URL-t</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="118"/>
        <source>Select a printer</source>
        <translation>Nyomtató kiválasztása</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="124"/>
        <source>Refresh</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="162"/>
        <source>Install Driver</source>
        <translation>Driver telepítés</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="190"/>
        <source>Address</source>
        <translation>Cím</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="192"/>
        <source>Enter an address</source>
        <translation>Írja be a címet</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="193"/>
        <source>Find</source>
        <translation>Találat</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="266"/>
        <source>URI</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="270"/>
        <source>Enter device URI</source>
        <translation>Írja be az eszköz URL-jét</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="281"/>
        <source>Examples:</source>
        <translation>Példák:</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="556"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="644"/>
        <source> (recommended)</source>
        <translation>(ajánlott)</translation>
    </message>
</context>
<context>
    <name>PrinterTestJob</name>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="369"/>
        <source>Check test page printing</source>
        <translation>Ellenőrizze a tesztoldal kinyomtatását</translation>
    </message>
    <message>
        <location filename="../vendor/ztroubleshoot.cpp" line="416"/>
        <source>Printing test page...</source>
        <translation>Tesztoldal nyomtatás...</translation>
    </message>
</context>
<context>
    <name>PrinterTestPageDialog</name>
    <message>
        <location filename="../ui/printertestpagedialog.cpp" line="63"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="103"/>
        <source>1 min ago</source>
        <translation>1 perce</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="106"/>
        <source>%1 mins ago</source>
        <translation>%1 perce</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="110"/>
        <source>1 hr ago</source>
        <translation>1 órája</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="112"/>
        <source>%1 hrs ago</source>
        <translation>%1 órája</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="117"/>
        <source>Yesterday</source>
        <translation>Tegnap</translation>
    </message>
    <message>
        <location filename="../ui/jobmanagerwindow.cpp" line="119"/>
        <source>%1 days ago</source>
        <translation>%1 napja</translation>
    </message>
    <message>
        <location filename="../ui/printersearchwindow.cpp" line="481"/>
        <source>(use %1 protocol)</source>
        <translation>(használjon %1 protokollt)</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="26"/>
        <source>Select a driver</source>
        <translation>Driver kiválasztás</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="32"/>
        <source>Driver</source>
        <translation>Driver</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="33"/>
        <source>Next</source>
        <translation>Következő</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="34"/>
        <source>Install Driver</source>
        <translation>Driver telepítés</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="36"/>
        <source>Drag a PPD file here 
 or</source>
        <translation>Húzza ise a PPD fájlt 
vagy</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="37"/>
        <source>Select a PPD file</source>
        <translation>Válassza ki a PPD fájlt</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="39"/>
        <source>Troubleshoot</source>
        <translation>Hibajavítás</translation>
    </message>
    <message>
        <location filename="../ui/uisourcestring.h" line="40"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../vendor/common.cpp" line="149"/>
        <source> not found, please ask the administrator for help</source>
        <translation>nem található, kérje a rendszergazda segítségét</translation>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="121"/>
        <source>Direct-attached Device</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/printerapplication.cpp" line="122"/>
        <source>File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="255"/>
        <source>Black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="256"/>
        <source>Blue</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="257"/>
        <source>Brown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="258"/>
        <source>Cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="259"/>
        <source>Dark-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="260"/>
        <source>Dark gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="261"/>
        <source>Dark-yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="262"/>
        <source>Dark yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="263"/>
        <source>Gold</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="264"/>
        <source>Gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="265"/>
        <source>Green</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="266"/>
        <source>Light-black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="267"/>
        <source>Light black</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="268"/>
        <source>Light-cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="269"/>
        <source>Light cyan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="270"/>
        <source>Light-gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="271"/>
        <source>Light gray</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="272"/>
        <source>Light-magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="273"/>
        <source>Light magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="274"/>
        <source>Magenta</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="275"/>
        <source>Orange</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="276"/>
        <source>Red</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="277"/>
        <source>Silver</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="278"/>
        <source>White</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="279"/>
        <source>Yellow</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ui/dprintersupplyshowdlg.cpp" line="280"/>
        <source>Waste</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RenamePrinterWindow</name>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="84"/>
        <source>Rename Printer</source>
        <translation>Nyomtató átnevezése</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="91"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="93"/>
        <source>Location</source>
        <translation>Hely</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="95"/>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="106"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../ui/renameprinterwindow.cpp" line="107"/>
        <source>Confirm</source>
        <translation>Megerősítés</translation>
    </message>
</context>
<context>
    <name>ServerSettingsWindow</name>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="85"/>
        <source>Basic Server Settings</source>
        <translation>Alapvető nyomtató beállítások</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="93"/>
        <source>Publish shared printers connected to this system</source>
        <translation>Nyilvános megosztott nyomtatók kapcsolódtak a rendszerhez</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="94"/>
        <source>Allow printing from the Internet</source>
        <translation>Nyomtatás engedélyezése az internetről</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="96"/>
        <source>Allow remote administration</source>
        <translation>Távoli elérés engedélyezése</translation>
    </message>
    <message>
        <location filename="../ui/dprintersshowwindow.h" line="98"/>
        <source>Save debugging information for troubleshooting</source>
        <translation>Mentse el a log fájlt a hibajavításhoz</translation>
    </message>
</context>
<context>
    <name>TroubleShootDialog</name>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="109"/>
        <source>Troubleshoot: </source>
        <translation>Hibajavítás:</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="134"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../ui/troubleshootdialog.cpp" line="165"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
</TS>